import { StudentSignupComponent } from './student/student-signup/student-signup.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { StudentDashboardComponent } from './student/student-dashboard/student-dashboard.component';
import { TeacherDashboardComponent } from './teacher/teacher-dashboard/teacher-dashboard.component';
import { AuthComponent } from './auth/auth.component';
import { StudentApproveComponent } from './admin/student-approve/student-approve.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'authorize',
    pathMatch: 'full'
  },
  { path: 'authorize', component: AuthComponent},
  { path: 'login', component: LoginComponent},
  { path: '**', redirectTo: 'authorize' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
