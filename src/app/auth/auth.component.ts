import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { DashboardService } from '../service/dashboard.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  constructor(
    private authenticationService: AuthenticationService,
    private dashboardService: DashboardService) { }

  ngOnInit() {
    if (this.authenticationService.authToken != null) {
      this.authenticationService.authorize().catch((err) => null).then(() => {
        this.dashboardService.toDashboard();
      });
    } else {
      this.dashboardService.toDashboard();
    }
  }

}
