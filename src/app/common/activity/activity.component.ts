import { Component, OnInit } from '@angular/core';
import { ActivityService } from 'src/app/service/activity.service';
import { ActivatedRoute } from '@angular/router';
import { Activity } from 'src/app/entity/activity';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { EnrollmentService } from 'src/app/service/enrollment.service';
import { Enrollment } from 'src/app/entity/enrollment';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit {
  activity: Activity;
  enrollment: Enrollment;

  constructor(
    private activityService: ActivityService,
    private authService: AuthenticationService,
    private enrollmentService: EnrollmentService,
    private route: ActivatedRoute) { }

  async ngOnInit() {
    let activityId = this.route.snapshot.params['activityId'];

    this.activity = await this.activityService.getActivity(activityId);

    if (this.isStudent) {
      this.enrollment = await this.enrollmentService.getStudentEnrollment(activityId);
    }
  }

  get isStudent(): boolean {
    return this.authService.isStudent;
  }

  get isTeacher(): boolean {
    return this.authService.isTeacher;
  }

  async enroll() {
    this.enrollment = await this.enrollmentService.enroll(this.activity.activityId);
  }

  isEnrollmentApproved(): boolean {
    return this.enrollment != null && !this.enrollment.isWaitingForApproval && this.enrollment.isApproved;
  }

  isEnrollmentRejected(): boolean {
    return this.enrollment != null && !this.enrollment.isWaitingForApproval && !this.enrollment.isApproved;
  }

  isEnrollmentPending(): boolean {
    return this.enrollment != null && this.enrollment.isWaitingForApproval;
  }

  isNoEnrollment(): boolean {
    return this.enrollment == null;
  }

}
