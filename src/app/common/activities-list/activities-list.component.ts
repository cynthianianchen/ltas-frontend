import { Component, OnInit, ViewChild, ModuleWithComponentFactories } from '@angular/core';
import { Activity } from 'src/app/entity/activity';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { ActivityService } from 'src/app/service/activity.service';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { Router } from '@angular/router';
import { Enrollment } from 'src/app/entity/enrollment';
import { EnrollmentService } from 'src/app/service/enrollment.service';
import * as moment from 'moment';

@Component({
  selector: 'app-activities-list',
  templateUrl: './activities-list.component.html',
  styleUrls: ['./activities-list.component.css']
})
export class ActivitiesListComponent implements OnInit {

  displayedColumns;
  dataSource: MatTableDataSource<Activity>;

  @ViewChild(MatPaginator, null) paginator: MatPaginator;
  @ViewChild(MatSort, null) sort: MatSort;
  activities: Activity[];
  enrollments: Enrollment[];

  private nameFilter = '';
  private dateFilter = '';

  constructor(
    private authService: AuthenticationService,
    private activityService: ActivityService,
    private enrollmentService: EnrollmentService,
    private router: Router) {

      this.displayedColumns = ['name', 'location', 'periodOfRegistration', 'date'].
        concat(!authService.isTeacher ? ['host'] : []).
        concat(authService.isStudent ? ['enrollment'] : []).
        concat(['view']);


  }

  async ngOnInit() {
    if (this.authService.isTeacher) {
      this.activities = await this.activityService.getAllActivitiesForTeacher();
    } else {
      this.activities = await this.activityService.getAll();

      if (this.authService.isStudent) {
        this.enrollments = await this.enrollmentService.getAllOwnEnrollments();
      }
    }

    this.dataSource = new MatTableDataSource(this.activities);

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.dataSource.filterPredicate = (data: Activity, filter: string) => {
      let result:boolean = true;

      if (filter.indexOf("$$$$") !== -1) {
        let [nameFilter, dateFilter] = filter.split("$$$$");

        if (nameFilter.length > 0) {
          result = result && (data.name != null && data.name.indexOf(nameFilter) != -1);
        }

        if (dateFilter.length > 0) {
          result = result && data.date != null && new Date(data.date).getTime() === new Date(dateFilter).getTime();
        }
      }

      return result;
    };
  }

  viewActivity(activityId: number) {
    this.router.navigate([`/common/activity/${activityId}`]);
  }

  applyActivityNameFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();

    this.nameFilter = filterValue;

    this.dataSource.filter = `${this.nameFilter}$$$$${this.dateFilter}`;
  }

  applyDateFilter(filterValue: string) {
    this.dateFilter = filterValue;

    this.dataSource.filter = `${this.nameFilter}$$$$${this.dateFilter}`;
  }

  private getEnrollmentByActivityId(activityId: number): Enrollment {
    return this.enrollments.find((enrollment) => enrollment.activity.activityId == activityId);
  }

  isEnrollmentApproved(activityId: number): boolean {
    const enrollment = this.getEnrollmentByActivityId(activityId);

    return enrollment != null && !enrollment.isWaitingForApproval && enrollment.isApproved;
  }

  isEnrollmentRejected(activityId: number): boolean {
    const enrollment = this.getEnrollmentByActivityId(activityId);

    return enrollment != null && !enrollment.isWaitingForApproval && !enrollment.isApproved;
  }

  isEnrollmentPending(activityId: number): boolean {
    const enrollment = this.getEnrollmentByActivityId(activityId);

    return enrollment != null && enrollment.isWaitingForApproval;
  }

  isNoEnrollment(activityId: number): boolean {
    const enrollment = this.getEnrollmentByActivityId(activityId);

    return enrollment == null;
  }

  formatDate(dateStr: string): string {
    return dateStr != null ? moment(new Date(dateStr)).format('MMMM Do YYYY') : '';
  }
}
