import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ActivityComponent } from './activity/activity.component';
import { AnyCanActivate } from './any-can-activate';
import { UpdateProfileComponent } from './update-profile/update-profile.component';

const routes: Routes = [
  { path: 'common/activity/:activityId', component: ActivityComponent, canActivate: [AnyCanActivate]},
  { path: 'common/updateProfile', component: UpdateProfileComponent, canActivate: [AnyCanActivate]},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AnyCanActivate]
})
export class CommonRoutingModule { }
