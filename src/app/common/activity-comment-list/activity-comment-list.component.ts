import { Component, OnInit, Input } from '@angular/core';
import { Activity } from 'src/app/entity/activity';
import { ActivityCommentService } from 'src/app/service/activity-comment.service';
import { ActivityComment } from 'src/app/entity/activity-comment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/service/authentication.service';

@Component({
  selector: 'app-activity-comment-list',
  templateUrl: './activity-comment-list.component.html',
  styleUrls: ['./activity-comment-list.component.css']
})
export class ActivityCommentListComponent implements OnInit {
  private _activity: Activity;
  comments: ActivityComment[];
  commentForm : FormGroup;

  constructor(
    private activityCommentService: ActivityCommentService,
    private formBuilder: FormBuilder,
    private authService: AuthenticationService) {
      this.commentForm = this.formBuilder.group({
        comment: ['', Validators.required]
      });
     }


  get isAdmin(): boolean {
    return this.authService.isAdmin;
  }

  get isStudentOrTeacher(): boolean {
    return this.authService.isStudent || this.authService.isTeacher;
  }

  @Input()
  get activity(): Activity {
    return this._activity;
  }

  set activity(value: Activity) {
    this._activity = value;
  }

  async ngOnInit() {
    this.comments = await this.activityCommentService.getComments(this.activity.activityId);
  }

  async onSubmit(commentText: string) {
    const newComment = await this.activityCommentService.newActivityComment(this.activity, commentText);

    this.commentForm.reset();

    for (let key in this.commentForm.controls) {
      this.commentForm.get(key).setErrors(null);
    }

    this.comments = [newComment].concat(this.comments);
  }

  async delete(comment: ActivityComment) {
    await this.activityCommentService.deleteActivityComment(comment.commentId);

    this.comments = this.comments.filter((_comment) => _comment !== comment);
  }

  getCommenterImage(comment: ActivityComment): string {
    let result: string = null;

    if (comment.postedByAdmin) {
      result = comment.postedByAdmin.image;
    } else if (comment.postedByTeacher) {
      result = comment.postedByTeacher.image;
    } else if (comment.postedByStudent) {
      result = comment.postedByStudent.image;
    }

    return result;
  }

  getCommenterName(comment: ActivityComment): string {
    let result: string = null;

    if (comment.postedByAdmin) {
      result = `${comment.postedByAdmin.name} ${comment.postedByAdmin.lastName}`;
    } else if (comment.postedByTeacher) {
      result = `${comment.postedByTeacher.name} ${comment.postedByTeacher.lastName}`;
    } else if (comment.postedByStudent) {
      result = `${comment.postedByStudent.name} ${comment.postedByStudent.lastName}`;
    }

    return result;
  }

  getCommenterType(comment: ActivityComment): string {
    let result: string = null;

    if (comment.postedByAdmin) {
      result = `Admin`;
    } else if (comment.postedByTeacher) {
      result = `Teacher`;
    } else if (comment.postedByStudent) {
      result = `Student`;
    }

    return result;
  }

  fieldHasError(fieldName: string, errorType: string):boolean {
    return this.commentForm.get(fieldName).hasError(errorType) &&
      (this.commentForm.get(fieldName).dirty || this.commentForm.get(fieldName).touched);
  }
}
