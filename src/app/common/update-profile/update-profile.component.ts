import { FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { Person } from 'src/app/entity/person';
import { PersonService } from 'src/app/service/person.service';
import { DashboardService } from 'src/app/service/dashboard.service';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.css']
})
export class UpdateProfileComponent implements OnInit {
  profileUpdateForm: FormGroup;
  unknownError: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private personService: PersonService,
    private authService: AuthenticationService,
    private dashboardService: DashboardService) {

    this.profileUpdateForm = this.formBuilder.group ({
      name: [authService.curPerson.name, Validators.required],
      lastname: [authService.curPerson.lastName, Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['']
    }, {validator: this.checkPasswords });
  }

  checkPasswords(group: FormGroup) {
    let pass = group.get('password');
    let confirmPass = group.get('confirmPassword');

    if (pass.value !== confirmPass.value) {
      confirmPass.setErrors({mustMatch: true});
    } else {
      confirmPass.setErrors(null);
    }
  }

  ngOnInit(): void {

  }

  fieldHasError(fieldName: string, errorType: string):boolean {
    return this.profileUpdateForm.get(fieldName).hasError(errorType) &&
      (this.profileUpdateForm.get(fieldName).dirty || this.profileUpdateForm.get(fieldName).touched);
  }

  submit({ value, valid }: { value: any, valid: boolean }) {
    const password: string = value.password;
    const person: Person = {
      email: this.authService.curPerson.email,
      name: value.name,
      lastName: value.lastName,
      image: null
    };

    this.personService.updateProfile(person, password).then(() => {
        this.dashboardService.toDashboard();
    }, (err: any) => {
      this.unknownError = true;
    });
  }


}
