import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatSidenavModule, MatTableModule, MatPaginatorModule, MatProgressBar, MatProgressBarModule, MatOptionModule, MatSelect, MatSelectModule, MatMenu, MatMenuModule, MatButtonModule, MatGridListModule, MatSortModule } from '@angular/material';
import { MatToolbarModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { NavigationComponent } from './navigation/navigation.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { TeacherDashboardComponent } from './teacher/teacher-dashboard/teacher-dashboard.component';
import { StudentDashboardComponent } from './student/student-dashboard/student-dashboard.component';
import { AuthComponent } from './auth/auth.component';
import { JwtInterceptorService } from './service/jwt-interceptor.service';
import { StudentSignupComponent } from './student/student-signup/student-signup.component';
import { StudentApproveComponent } from './admin/student-approve/student-approve.component';
import { AdminRoutingModule } from './admin/admin-routing.module';
import { StudentRoutingModule } from './student/student-routing.module';
import { TeacherRoutingModule } from './teacher/teacher-routing.module';
import { NewActivityComponent } from './admin/new-activity/new-activity.component';
import { ActivitiesListComponent } from './common/activities-list/activities-list.component';
import { CommonRoutingModule } from './common/common-routing.module';
import { ActivityComponent } from './common/activity/activity.component';
import { EnrollmentApproveComponent } from './teacher/enrollment-approve/enrollment-approve.component';
import { ActivityCommentListComponent } from './common/activity-comment-list/activity-comment-list.component';
import { MatProgressSpinnerModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { UpdateProfileComponent } from './common/update-profile/update-profile.component';
import { MatFileUploadModule } from "mat-file-upload";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavigationComponent,
    TeacherDashboardComponent,
    StudentDashboardComponent,
    AuthComponent,
    StudentSignupComponent,
    AdminDashboardComponent,
    StudentApproveComponent,
    NewActivityComponent,
    EnrollmentApproveComponent,
    ActivitiesListComponent,
    ActivityComponent,
    ActivityCommentListComponent,
    UpdateProfileComponent
  ],
  imports: [
    BrowserModule,
    AdminRoutingModule,
    StudentRoutingModule,
    TeacherRoutingModule,
    CommonRoutingModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatToolbarModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatMenuModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatInputModule,
    HttpClientModule,
    MatSelectModule,
    MatOptionModule,
    MatFileUploadModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSortModule

  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true},
  ],
  bootstrap: [AppComponent],
  entryComponents: [NewActivityComponent]
})
export class AppModule { }
