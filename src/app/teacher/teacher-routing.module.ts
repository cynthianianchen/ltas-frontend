import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { TeacherCanActivate } from './teacher-can-activate';
import { TeacherDashboardComponent } from './teacher-dashboard/teacher-dashboard.component';

const routes: Routes = [
  { path: 'teacher/dashboard', component: TeacherDashboardComponent, canActivate: [TeacherCanActivate]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [TeacherCanActivate]
})
export class TeacherRoutingModule { }
