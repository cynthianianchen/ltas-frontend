import { Component, OnInit, ViewChild, AfterViewInit, Input } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Enrollment } from 'src/app/entity/enrollment';
import { EnrollmentService } from 'src/app/service/enrollment.service';

@Component({
  selector: 'app-enrollment-approve',
  templateUrl: './enrollment-approve.component.html',
  styleUrls: ['./enrollment-approve.component.css'],
})
export class EnrollmentApproveComponent implements OnInit {

  private _activityId: number;

  displayedColumns = ['studentName', 'approve', 'reject', 'undo'];
  dataSource: MatTableDataSource<Enrollment>;

  processingEnrollments: Set<Enrollment> = new Set<Enrollment>();

  @ViewChild(MatPaginator, null) paginator: MatPaginator;
  @ViewChild(MatSort, null) sort: MatSort;

  @Input()
  get activityId(): number {
    return this._activityId;
  }

  set activityId(value: number) {
    this._activityId = value;
  }

  constructor(private enrollmentService: EnrollmentService) {

  }

  isProcessing(enrollment: Enrollment): boolean {
    return this.processingEnrollments.has(enrollment);
  }

  approve(enrollment: Enrollment) {
    this.processingEnrollments.add(enrollment);

    this.enrollmentService.approveEnrollment(enrollment.enrollmentId).then(() => {
      enrollment.isWaitingForApproval = false;
      enrollment.isApproved = true;

      this.processingEnrollments.delete(enrollment);
    }, () => {
      this.processingEnrollments.delete(enrollment);
    });
  }

  reject(enrollment: Enrollment) {
    this.processingEnrollments.add(enrollment);

    this.enrollmentService.rejectEnrollment(enrollment.enrollmentId).then(() => {
      enrollment.isWaitingForApproval = false;
      enrollment.isApproved = false;

      this.processingEnrollments.delete(enrollment);
    }, () => {
      this.processingEnrollments.delete(enrollment);
    });
  }

  undo(enrollment: Enrollment) {
    enrollment.isWaitingForApproval = true;
  }

  ngOnInit() {
    this.enrollmentService.getActivityEnrollments(this.activityId).then((enrollment) => {
      this.dataSource = new MatTableDataSource(enrollment);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
}
