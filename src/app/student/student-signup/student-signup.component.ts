import { FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Student } from 'src/app/entity/student';
import { StudentService } from 'src/app/service/student.service';
import { Router } from '@angular/router';

import { FileUploadService } from 'src/app/service/file-upload.service';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-student-signup',
  templateUrl: './student-signup.component.html',
  styleUrls: ['./student-signup.component.css']
})
export class StudentSignupComponent implements OnInit {
  studentForm: FormGroup;
  uploadedUrl: string;
  progress:number;

  form: any;
  unknownError: boolean = false;
  userAlreadyExistsError: boolean = false;


  constructor(
    private formBuilder: FormBuilder,
    private studentService: StudentService,
    private router: Router,
    private fileUploadService:FileUploadService) {

    this.studentForm = this.formBuilder.group ({
      studentId: [null, Validators.compose([Validators.required, Validators.maxLength(10), Validators.pattern('[0-9]+')])],
      name: [null,Validators.required],
      lastname: [null,Validators.required],
      email: [null,Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required],
      image: [''],
      confirmPassword: ['']
    }, {validator: this.checkPasswords });
  }

  checkPasswords(group: FormGroup) {
    let pass = group.get('password');
    let confirmPass = group.get('confirmPassword');

    if (pass.value !== confirmPass.value) {
      confirmPass.setErrors({mustMatch: true});
    } else {
      confirmPass.setErrors(null);
    }
  }

  fieldHasError(fieldName: string, errorType: string):boolean {
    return this.studentForm.get(fieldName).hasError(errorType) &&
      (this.studentForm.get(fieldName).dirty || this.studentForm.get(fieldName).touched);
  }

  submit({ value, valid }: { value: any, valid: boolean }) {
    const password: string = value.password;
    const student: Student = {
      email: value.email,
      studentId: value.studentId,
      name: value.name,
      lastName: value.lastName,
      image:value.image,
    };

    this.studentService.signup(student, password).then(() => {
        this.router.navigate(['/login'], {queryParams: {cmd: 'showSignupSuccess'}});
    }, (err: any) => {      
      if (err.error != null && err.error.message == "User with this email already exists") {
        this.userAlreadyExistsError = true;
      } else {
        this.unknownError = true;
      }
    });
  }

  onUploadClicked(files?: FileList) {
    console.log(typeof(files));
    console.log(files.item(0));
    const uploadedFile = files.item(0);
    this.progress = 0;
    this.fileUploadService.uploadFile(uploadedFile)
      .subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Sent:
            console.log('Request has been made!');
            break;
          case HttpEventType.ResponseHeader:
            console.log('Response header has been received!');
            break;
          case HttpEventType.UploadProgress:
            this.progress = Math.round(event.loaded / event.total * 100);
            console.log(`Uploaded! ${this.progress}%`);
            break;
          case HttpEventType.Response:
            console.log('User successfully created!', event.body);
            this.uploadedUrl = event.body;
            this.studentForm.patchValue({
              image: this.uploadedUrl
            });
            this.studentForm.get('image').updateValueAndValidity();
            setTimeout(() => {
              this.progress = 0;
            }, 1500);
          }
        });
  }


  ngOnInit(): void {

   }

}
