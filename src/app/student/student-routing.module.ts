import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { StudentCanActivate } from './student-can-activate';
import { StudentSignupComponent } from './student-signup/student-signup.component';
import { StudentDashboardComponent } from './student-dashboard/student-dashboard.component';

const routes: Routes = [
  { path: 'student/signup', component: StudentSignupComponent},
  { path: 'student/dashboard', component: StudentDashboardComponent, canActivate: [StudentCanActivate]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [StudentCanActivate]
})
export class StudentRoutingModule { }
