import { Injectable } from '@angular/core';
import { Student } from '../entity/student';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http: HttpClient) { }

  signup(student: Student, password: string): Promise<void> {
    return this.http.post<void>('/api/public/students/register', {
        student: student,
        password: password
      }).toPromise();
  }

  getAllWaitingForApprovalStudents():Promise<Student[]> {
    return this.http.get<Student[]>('/api/admin/students/loadWaitingForApproval').toPromise();
  }

  approve(email: string): Promise<void> {
    return this.http.post<void>('/api/admin/students/approve', email).toPromise();
  }

  reject(email: string): Promise<void> {
    return this.http.post<void>('/api/admin/students/reject', email).toPromise();
  }
}
