/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ActivityCommentService } from './activity-comment.service';

describe('Service: ActivityComment', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActivityCommentService]
    });
  });

  it('should ...', inject([ActivityCommentService], (service: ActivityCommentService) => {
    expect(service).toBeTruthy();
  }));
});
