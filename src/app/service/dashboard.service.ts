import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  toDashboard(): void {
    if (this.authenticationService.isAdmin) {
      this.router.navigate(['/admin/dashboard']);
    } else if (this.authenticationService.isStudent) {
      this.router.navigate(['/student/dashboard']);
    } else if (this.authenticationService.isTeacher) {
      this.router.navigate(['/teacher/dashboard']);
    } else {
      this.router.navigate(['/login']);
    }
  }
}
