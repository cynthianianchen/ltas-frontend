import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Activity } from '../entity/activity';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ActivityService {
  constructor(private http: HttpClient) { }

  newActivity(activity: Activity): Promise<void> {
    return this.http.post<void>('/api/admin/activities/new', activity).toPromise();
  }

  getAll(): Promise<Activity[]> {
    return this.http.get<Activity[]>('/api/public/activities').toPromise();
  }

  getAllActivitiesForTeacher(): Promise<Activity[]> {
    return this.http.get<Activity[]>('/api/teacher/activities/getAllOwn').toPromise();
  }

  getAllActivities():Promise<Activity[]> {
    return this.http.get('/api/studentAndTeacher/activities').pipe(
      map((result) => result as Activity[])).toPromise();
  }

  getActivity(activityId: number): Promise<Activity> {
    return this.http.get<Activity>(`/api/public/activities/getActivity/${activityId}`).toPromise();
  }

  

}
