import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Enrollment } from '../entity/enrollment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EnrollmentService {
  constructor(private http: HttpClient) { }

  getStudentEnrollment(activityId: number): Promise<Enrollment> {
    return this.http.get<Enrollment>(`/api/student/enrollments/get/${activityId}`).toPromise();
  }

  enroll(activityId: number): Promise<Enrollment> {
    return this.http.get<Enrollment>(`/api/student/enrollments/enroll/${activityId}`).toPromise();
  }

  getAllOwnEnrollments(): Promise<Enrollment[]> {
    return this.http.get<Enrollment[]>('/api/student/enrollments').toPromise();
  }

  getActivityEnrollments(activityId: number): Promise<Enrollment[]> {
    return this.http.get<Enrollment[]>(`/api/teacher/enrollments/activity/${activityId}`).toPromise();
  }

  approveEnrollment(enrollmentId: string): Promise<void> {
    return this.http.get<void>(`/api/teacher/enrollments/approve/${enrollmentId}`).toPromise();
  }

  rejectEnrollment(enrollmentId: string): Promise<void> {
    return this.http.get<void>(`/api/teacher/enrollments/reject/${enrollmentId}`).toPromise();
  }

}
