import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Person } from '../entity/person';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  constructor(private http: HttpClient) { }

  updateProfile(person: Person, password: string): Promise<void> {
    return this.http.post<void>('/api/public/persons/update', {
        person: person,
        password: password
      }).toPromise();
  }

}
