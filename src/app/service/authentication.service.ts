import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Person } from '../entity/person';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private _curPerson: Person;
  private _curUserType: string;
  private _isAuthorized: boolean = false;

  constructor(private http: HttpClient) { }

  get curPerson(): Person {
    return this._curPerson;
  }

  get curUserType(): string {
    return this._curUserType;
  }

  get authToken(): string {
    return localStorage.getItem('authToken');
  }

  get isAuthorized(): boolean {
    return this._isAuthorized;
  }

  private saveAuthToken(authToken: string) {
    localStorage.setItem('authToken', authToken);
  }

  private resetAuthToken() {
    localStorage.removeItem('authToken');
  }

  get isAdmin(): boolean {
    return this._curUserType === 'ADMIN';
  }

  get isStudent(): boolean {
    return this._curUserType === 'STUDENT';
  }

  get isTeacher(): boolean {
    return this._curUserType === 'TEACHER';
  }

  authorize(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.http.get<any>('/api/public/authorize').
        subscribe((data: any) => {
          if (data.userType) {
            this._isAuthorized = true;
            this._curPerson = data.person as Person;
            this._curUserType = data.userType;

            resolve();
          } else {
            reject(new Error('No data'));
          }
        }, (err) => {
          reject(new Error(err.error));
        });
    });
  }

  login(email: string, password: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.resetAuthToken();

      this.http.post<any>('/api/public/auth', {
        email: email,
        password: password
      }).subscribe((data: any) => {
        if (data.token) {
          this.saveAuthToken(data.token);

          this._isAuthorized = true;
          this._curPerson = data.person as Person;
          this._curUserType = data.userType;

          resolve();
        } else {
          reject(new Error('No data'));
        }
      }, (err: any) => {
        reject(new Error(err.error.message));
      });
    });
  }

  logout() {
    this.resetAuthToken();

    this._isAuthorized = false;
    this._curPerson = null;
    this._curUserType = null;
  }
}
