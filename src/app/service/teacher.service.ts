import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Teacher } from '../entity/teacher';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  constructor(private http: HttpClient) { }

  getAll(): Promise<Teacher[]> {
    return this.http.get<Teacher[]>('/api/admin/teachers').toPromise();
  }

}
