import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivityComment } from '../entity/activity-comment';
import { Activity } from '../entity/activity';

@Injectable({
  providedIn: 'root'
})
export class ActivityCommentService {

  constructor(private http: HttpClient) {

  }

  newActivityComment(activity: Activity, text: string): Promise<ActivityComment> {
    const activityComment:ActivityComment = {
      commentId: null,
      activity: activity,
      postedByStudent: null,
      postedByTeacher: null,
      postedByAdmin: null,
      text: text,
      postedOn: null
    };

    return this.http.post<ActivityComment>('/api/studentAndTeacher/activityComments', activityComment).toPromise();
  }

  getComments(activityId: number): Promise<ActivityComment[]> {
    return this.http.get<ActivityComment[]>(`/api/public/activityComments/${activityId}`).toPromise();
  }

  deleteActivityComment(activityCommentId: number):Promise<void> {
    return this.http.delete<void>(`/api/admin/activityComments/${activityCommentId}`).toPromise();
  }
}
