import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { Observable } from 'rxjs';
import { from } from 'rxjs';

@Injectable()
export class AdminCanActivate implements CanActivate {
  constructor(private authenticationService: AuthenticationService, private router: Router) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    let result: Observable<boolean | UrlTree> | boolean | UrlTree = null;

    if (this.authenticationService.isAuthorized) {
      if (this.authenticationService.isAdmin) {
        result = true;
      } else {
        result = this.router.parseUrl('/authorize');
      }
    } else {
      result = from(this.authenticationService.authorize().then(() => {
        let innerResult: UrlTree | boolean = null;

        if (this.authenticationService.isAdmin) {
          innerResult = true;
        } else {
          innerResult = this.router.parseUrl('/authorize');
        }

        return innerResult;
      }));
    }

    return result;
  }
}
