import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Student } from 'src/app/entity/student';
import { StudentService } from 'src/app/service/student.service';

@Component({
  selector: 'app-student-approve',
  templateUrl: './student-approve.component.html',
  styleUrls: ['./student-approve.component.css']
})
export class StudentApproveComponent implements OnInit {
  displayedColumns = ['image', 'studentId', 'email', 'name', 'lastName', 'approve', 'reject', 'undo'];
  dataSource: MatTableDataSource<Student>;

  studentProcessingStates: Map<Student, string> = new Map<Student, string>();

  @ViewChild(MatPaginator, null) paginator: MatPaginator;
  @ViewChild(MatSort, null) sort: MatSort;

  constructor(private studentService: StudentService) {

  }

  getStudentProcessingState(student: Student): string {
    return this.studentProcessingStates.get(student) || null;
  }

  approve(student: Student) {
    this.studentProcessingStates.set(student, 'processing');

    this.studentService.approve(student.email).then(() => {
      this.studentProcessingStates.set(student, 'approved');
    }, () => {
      this.studentProcessingStates.set(student, null);
    });
  }

  reject(student: Student) {
    this.studentProcessingStates.set(student, 'processing');

    this.studentService.reject(student.email).then(() => {
      this.studentProcessingStates.set(student, 'rejected');
    }, () => {
      this.studentProcessingStates.set(student, null);
    });
  }

  undo(student: Student) {
    this.studentProcessingStates.set(student, null);
  }

  ngOnInit() {
    this.studentService.getAllWaitingForApprovalStudents().then((students) => {
      this.dataSource = new MatTableDataSource(students);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
}
