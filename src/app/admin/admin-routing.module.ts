import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { StudentApproveComponent } from './student-approve/student-approve.component';
import { AdminCanActivate } from './admin-can-activate';
import { NewActivityComponent } from './new-activity/new-activity.component';

const routes: Routes = [
  { path: 'admin/dashboard', component: AdminDashboardComponent, canActivate: [AdminCanActivate]},
  { path: 'admin/new-activity', component: NewActivityComponent, canActivate: [AdminCanActivate]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AdminCanActivate]
})
export class AdminRoutingModule { }
