import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  isActivityCreated: boolean = false;

  constructor(private router: Router,private route: ActivatedRoute,) { }

  ngOnInit() : void {
    if (this.route.snapshot.queryParams['cmd'] === 'showCreateActivitySuccess') {
      this.isActivityCreated = true;
    }
  }

  newActivity() {
    this.router.navigate(['admin/new-activity']);
  }
  createNewActivity(){

  }

}
