import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { TeacherService } from 'src/app/service/teacher.service';
import { Teacher } from 'src/app/entity/teacher';
import { Activity } from 'src/app/entity/activity';
import { ActivityService } from 'src/app/service/activity.service';
import { Router } from '@angular/router';

import { FileUploadService } from 'src/app/service/file-upload.service';
import { environment } from 'src/environments/environment';
import { HttpEvent, HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-new-activity',
  templateUrl: './new-activity.component.html',
  styleUrls: ['./new-activity.component.css']
})
export class NewActivityComponent implements OnInit {
  newActivityForm: FormGroup;
  teachers: Teacher[];

  uploadedUrl: string;
  progress: number;
  form: any;
  isActivityCreated: boolean = false;
  unknownError: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private teacherService: TeacherService,
    private activityService: ActivityService,
    private fileUploadService: FileUploadService,
    private router: Router
    ) {
      this.newActivityForm = this.formBuilder.group({
        name: ['',Validators.required],
        location: ['',Validators.required],
        description: ['',Validators.required],
        periodOfRegistration: ['',Validators.compose([Validators.required, Validators.pattern('[0-9]+')])],
        date: ['',Validators.required],
        host: ['',Validators.required],
        image: ['']
      });

      (<any>window).router = this.router;
    }

  ngOnInit() {
    this.teacherService.getAll().then((teachers) => {
      this.teachers = teachers;
    });

  }

  fieldHasError(fieldName: string, errorType: string):boolean {
  return this.newActivityForm.get(fieldName).hasError(errorType) &&
    (this.newActivityForm.get(fieldName).dirty || this.newActivityForm.get(fieldName).touched);
  }


  async submit({ value, valid }: { value: any, valid: boolean }) {
    let activity: Activity = {
      activityId: null,
      name: value.name,
      location: value.location,
      description: value.description,
      periodOfRegistration: value.periodOfRegistration,
      date: value.date,
      image: value.image,
      host: this.teachers.find((teacher) => teacher.email === value.host)
    };

    this.activityService.newActivity(activity).then(() => {
      this.router.navigate(['/admin/dashboard'], {queryParams: {cmd: 'showCreateActivitySuccess'}});
    }, (err: any) => {
      this.unknownError = true;
    });
  }

  onUploadClicked(files?: FileList) {
    console.log(typeof(files));
    console.log(files.item(0));
    const uploadedFile = files.item(0);
    this.progress = 0;
    this.fileUploadService.uploadFile(uploadedFile)
      .subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Sent:
            console.log('Request has been made!');
            break;
          case HttpEventType.ResponseHeader:
            console.log('Response header has been received!');
            break;
          case HttpEventType.UploadProgress:
            this.progress = Math.round(event.loaded / event.total * 100);
            console.log(`Uploaded! ${this.progress}%`);
            break;
          case HttpEventType.Response:
            console.log('User successfully created!', event.body);
            this.uploadedUrl = event.body;
            this.newActivityForm.patchValue({
              image: this.uploadedUrl
            });
            this.newActivityForm.get('image').updateValueAndValidity();
            setTimeout(() => {
              this.progress = 0;
            }, 1500);
          }
        });
  }

  onSelectedFilesChanged(files?: FileList){

  }



}
