import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { Person } from '../entity/person';
import { DashboardService } from '../service/dashboard.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  get isAuthorized(): boolean {
    return this.authService.curPerson != null;
  }

  get isAdmin(): boolean {
    return this.authService.isAdmin;
  }

  get isTeacher(): boolean {
    return this.authService.isTeacher;
  }

  get isStudent(): boolean {
    return this.authService.isStudent;
  }

  get person(): Person {
    return this.authService.curPerson;
  }

  constructor(
    private authService: AuthenticationService,
    private dashboardService: DashboardService,
    private router: Router) {

  }

  toDashboard() {
    this.dashboardService.toDashboard();
  }

  logout() {
    this.authService.logout();

    this.dashboardService.toDashboard();
  }

  edit() {
    this.router.navigate(['/common/updateProfile']);
  }

  ngOnInit() {
  }

}
