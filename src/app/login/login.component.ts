import { DashboardService } from './../service/dashboard.service';
import { AuthenticationService } from '../service/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = this.fb.group({
    email: [''],
    password: ['']
  });
  userDisabledError: boolean;
  invalidCredentialsError: boolean;
  registerSuccess: boolean = false;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private dashboardService: DashboardService,
    private authenticationService: AuthenticationService) { }

  ngOnInit(): void {

    if (this.route.snapshot.queryParams['cmd'] === 'showSignupSuccess') {
      this.registerSuccess = true;
    }

   }

  async onSubmit({ value, valid }: { value: any, valid: boolean }) {
    try {
      await this.authenticationService.login(value.email, value.password);

      this.dashboardService.toDashboard();
    } catch (err) {
      switch (err.message) {
        case 'Invalid credentials':
          this.invalidCredentialsError = true;
          this.userDisabledError = false;
          break;
        case 'User disabled':
          this.invalidCredentialsError = false;
          this.userDisabledError = true;
          break;
      }
    }
  }

  studentSignUp() {
    this.router.navigate(['student/signup']);
  }

  signup() {

  }
}
