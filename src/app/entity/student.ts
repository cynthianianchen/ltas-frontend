import { Person } from './person';

export interface Student extends Person {
  studentId: number;
  image:string;
}
