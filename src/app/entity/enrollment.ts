import { Activity } from './activity';
import { Student } from './student';

export interface Enrollment {
  enrollmentId: string;

  activity: Activity;
  student: Student;

  isApproved: boolean;
  isWaitingForApproval: boolean;
}
