export interface Person {
  email: string;
  name: string;
  lastName: string;
  image: string;
}
