import { Activity } from './activity';
import { Student } from './student';
import { Teacher } from './teacher';
import { Admin } from './admin';

export interface ActivityComment {
  commentId: number;
  activity: Activity;

  postedByStudent: Student;
  postedByTeacher: Teacher;
  postedByAdmin: Admin;

  text: string;
  postedOn: Date;
}
