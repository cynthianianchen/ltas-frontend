import { Teacher } from './teacher';

export interface Activity {
  activityId: number;
  name: string;
  location: string;
  description: string;
  periodOfRegistration: string;
  date: string;
  host: Teacher;
  image:string;
}
