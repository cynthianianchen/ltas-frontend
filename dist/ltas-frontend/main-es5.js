var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
        /***/ "./$$_lazy_route_resource lazy recursive": 
        /*!******************************************************!*\
          !*** ./$$_lazy_route_resource lazy namespace object ***!
          \******************************************************/
        /*! no static exports found */
        /***/ (function (module, exports) {
            function webpackEmptyAsyncContext(req) {
                // Here Promise.resolve().then() is used instead of new Promise() to prevent
                // uncaught exception popping up in devtools
                return Promise.resolve().then(function () {
                    var e = new Error("Cannot find module '" + req + "'");
                    e.code = 'MODULE_NOT_FOUND';
                    throw e;
                });
            }
            webpackEmptyAsyncContext.keys = function () { return []; };
            webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
            module.exports = webpackEmptyAsyncContext;
            webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
            /***/ 
        }),
        /***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$": 
        /*!**************************************************!*\
          !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
          \**************************************************/
        /*! no static exports found */
        /***/ (function (module, exports, __webpack_require__) {
            var map = {
                "./af": "./node_modules/moment/locale/af.js",
                "./af.js": "./node_modules/moment/locale/af.js",
                "./ar": "./node_modules/moment/locale/ar.js",
                "./ar-dz": "./node_modules/moment/locale/ar-dz.js",
                "./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
                "./ar-kw": "./node_modules/moment/locale/ar-kw.js",
                "./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
                "./ar-ly": "./node_modules/moment/locale/ar-ly.js",
                "./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
                "./ar-ma": "./node_modules/moment/locale/ar-ma.js",
                "./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
                "./ar-sa": "./node_modules/moment/locale/ar-sa.js",
                "./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
                "./ar-tn": "./node_modules/moment/locale/ar-tn.js",
                "./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
                "./ar.js": "./node_modules/moment/locale/ar.js",
                "./az": "./node_modules/moment/locale/az.js",
                "./az.js": "./node_modules/moment/locale/az.js",
                "./be": "./node_modules/moment/locale/be.js",
                "./be.js": "./node_modules/moment/locale/be.js",
                "./bg": "./node_modules/moment/locale/bg.js",
                "./bg.js": "./node_modules/moment/locale/bg.js",
                "./bm": "./node_modules/moment/locale/bm.js",
                "./bm.js": "./node_modules/moment/locale/bm.js",
                "./bn": "./node_modules/moment/locale/bn.js",
                "./bn.js": "./node_modules/moment/locale/bn.js",
                "./bo": "./node_modules/moment/locale/bo.js",
                "./bo.js": "./node_modules/moment/locale/bo.js",
                "./br": "./node_modules/moment/locale/br.js",
                "./br.js": "./node_modules/moment/locale/br.js",
                "./bs": "./node_modules/moment/locale/bs.js",
                "./bs.js": "./node_modules/moment/locale/bs.js",
                "./ca": "./node_modules/moment/locale/ca.js",
                "./ca.js": "./node_modules/moment/locale/ca.js",
                "./cs": "./node_modules/moment/locale/cs.js",
                "./cs.js": "./node_modules/moment/locale/cs.js",
                "./cv": "./node_modules/moment/locale/cv.js",
                "./cv.js": "./node_modules/moment/locale/cv.js",
                "./cy": "./node_modules/moment/locale/cy.js",
                "./cy.js": "./node_modules/moment/locale/cy.js",
                "./da": "./node_modules/moment/locale/da.js",
                "./da.js": "./node_modules/moment/locale/da.js",
                "./de": "./node_modules/moment/locale/de.js",
                "./de-at": "./node_modules/moment/locale/de-at.js",
                "./de-at.js": "./node_modules/moment/locale/de-at.js",
                "./de-ch": "./node_modules/moment/locale/de-ch.js",
                "./de-ch.js": "./node_modules/moment/locale/de-ch.js",
                "./de.js": "./node_modules/moment/locale/de.js",
                "./dv": "./node_modules/moment/locale/dv.js",
                "./dv.js": "./node_modules/moment/locale/dv.js",
                "./el": "./node_modules/moment/locale/el.js",
                "./el.js": "./node_modules/moment/locale/el.js",
                "./en-SG": "./node_modules/moment/locale/en-SG.js",
                "./en-SG.js": "./node_modules/moment/locale/en-SG.js",
                "./en-au": "./node_modules/moment/locale/en-au.js",
                "./en-au.js": "./node_modules/moment/locale/en-au.js",
                "./en-ca": "./node_modules/moment/locale/en-ca.js",
                "./en-ca.js": "./node_modules/moment/locale/en-ca.js",
                "./en-gb": "./node_modules/moment/locale/en-gb.js",
                "./en-gb.js": "./node_modules/moment/locale/en-gb.js",
                "./en-ie": "./node_modules/moment/locale/en-ie.js",
                "./en-ie.js": "./node_modules/moment/locale/en-ie.js",
                "./en-il": "./node_modules/moment/locale/en-il.js",
                "./en-il.js": "./node_modules/moment/locale/en-il.js",
                "./en-nz": "./node_modules/moment/locale/en-nz.js",
                "./en-nz.js": "./node_modules/moment/locale/en-nz.js",
                "./eo": "./node_modules/moment/locale/eo.js",
                "./eo.js": "./node_modules/moment/locale/eo.js",
                "./es": "./node_modules/moment/locale/es.js",
                "./es-do": "./node_modules/moment/locale/es-do.js",
                "./es-do.js": "./node_modules/moment/locale/es-do.js",
                "./es-us": "./node_modules/moment/locale/es-us.js",
                "./es-us.js": "./node_modules/moment/locale/es-us.js",
                "./es.js": "./node_modules/moment/locale/es.js",
                "./et": "./node_modules/moment/locale/et.js",
                "./et.js": "./node_modules/moment/locale/et.js",
                "./eu": "./node_modules/moment/locale/eu.js",
                "./eu.js": "./node_modules/moment/locale/eu.js",
                "./fa": "./node_modules/moment/locale/fa.js",
                "./fa.js": "./node_modules/moment/locale/fa.js",
                "./fi": "./node_modules/moment/locale/fi.js",
                "./fi.js": "./node_modules/moment/locale/fi.js",
                "./fo": "./node_modules/moment/locale/fo.js",
                "./fo.js": "./node_modules/moment/locale/fo.js",
                "./fr": "./node_modules/moment/locale/fr.js",
                "./fr-ca": "./node_modules/moment/locale/fr-ca.js",
                "./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
                "./fr-ch": "./node_modules/moment/locale/fr-ch.js",
                "./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
                "./fr.js": "./node_modules/moment/locale/fr.js",
                "./fy": "./node_modules/moment/locale/fy.js",
                "./fy.js": "./node_modules/moment/locale/fy.js",
                "./ga": "./node_modules/moment/locale/ga.js",
                "./ga.js": "./node_modules/moment/locale/ga.js",
                "./gd": "./node_modules/moment/locale/gd.js",
                "./gd.js": "./node_modules/moment/locale/gd.js",
                "./gl": "./node_modules/moment/locale/gl.js",
                "./gl.js": "./node_modules/moment/locale/gl.js",
                "./gom-latn": "./node_modules/moment/locale/gom-latn.js",
                "./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
                "./gu": "./node_modules/moment/locale/gu.js",
                "./gu.js": "./node_modules/moment/locale/gu.js",
                "./he": "./node_modules/moment/locale/he.js",
                "./he.js": "./node_modules/moment/locale/he.js",
                "./hi": "./node_modules/moment/locale/hi.js",
                "./hi.js": "./node_modules/moment/locale/hi.js",
                "./hr": "./node_modules/moment/locale/hr.js",
                "./hr.js": "./node_modules/moment/locale/hr.js",
                "./hu": "./node_modules/moment/locale/hu.js",
                "./hu.js": "./node_modules/moment/locale/hu.js",
                "./hy-am": "./node_modules/moment/locale/hy-am.js",
                "./hy-am.js": "./node_modules/moment/locale/hy-am.js",
                "./id": "./node_modules/moment/locale/id.js",
                "./id.js": "./node_modules/moment/locale/id.js",
                "./is": "./node_modules/moment/locale/is.js",
                "./is.js": "./node_modules/moment/locale/is.js",
                "./it": "./node_modules/moment/locale/it.js",
                "./it-ch": "./node_modules/moment/locale/it-ch.js",
                "./it-ch.js": "./node_modules/moment/locale/it-ch.js",
                "./it.js": "./node_modules/moment/locale/it.js",
                "./ja": "./node_modules/moment/locale/ja.js",
                "./ja.js": "./node_modules/moment/locale/ja.js",
                "./jv": "./node_modules/moment/locale/jv.js",
                "./jv.js": "./node_modules/moment/locale/jv.js",
                "./ka": "./node_modules/moment/locale/ka.js",
                "./ka.js": "./node_modules/moment/locale/ka.js",
                "./kk": "./node_modules/moment/locale/kk.js",
                "./kk.js": "./node_modules/moment/locale/kk.js",
                "./km": "./node_modules/moment/locale/km.js",
                "./km.js": "./node_modules/moment/locale/km.js",
                "./kn": "./node_modules/moment/locale/kn.js",
                "./kn.js": "./node_modules/moment/locale/kn.js",
                "./ko": "./node_modules/moment/locale/ko.js",
                "./ko.js": "./node_modules/moment/locale/ko.js",
                "./ku": "./node_modules/moment/locale/ku.js",
                "./ku.js": "./node_modules/moment/locale/ku.js",
                "./ky": "./node_modules/moment/locale/ky.js",
                "./ky.js": "./node_modules/moment/locale/ky.js",
                "./lb": "./node_modules/moment/locale/lb.js",
                "./lb.js": "./node_modules/moment/locale/lb.js",
                "./lo": "./node_modules/moment/locale/lo.js",
                "./lo.js": "./node_modules/moment/locale/lo.js",
                "./lt": "./node_modules/moment/locale/lt.js",
                "./lt.js": "./node_modules/moment/locale/lt.js",
                "./lv": "./node_modules/moment/locale/lv.js",
                "./lv.js": "./node_modules/moment/locale/lv.js",
                "./me": "./node_modules/moment/locale/me.js",
                "./me.js": "./node_modules/moment/locale/me.js",
                "./mi": "./node_modules/moment/locale/mi.js",
                "./mi.js": "./node_modules/moment/locale/mi.js",
                "./mk": "./node_modules/moment/locale/mk.js",
                "./mk.js": "./node_modules/moment/locale/mk.js",
                "./ml": "./node_modules/moment/locale/ml.js",
                "./ml.js": "./node_modules/moment/locale/ml.js",
                "./mn": "./node_modules/moment/locale/mn.js",
                "./mn.js": "./node_modules/moment/locale/mn.js",
                "./mr": "./node_modules/moment/locale/mr.js",
                "./mr.js": "./node_modules/moment/locale/mr.js",
                "./ms": "./node_modules/moment/locale/ms.js",
                "./ms-my": "./node_modules/moment/locale/ms-my.js",
                "./ms-my.js": "./node_modules/moment/locale/ms-my.js",
                "./ms.js": "./node_modules/moment/locale/ms.js",
                "./mt": "./node_modules/moment/locale/mt.js",
                "./mt.js": "./node_modules/moment/locale/mt.js",
                "./my": "./node_modules/moment/locale/my.js",
                "./my.js": "./node_modules/moment/locale/my.js",
                "./nb": "./node_modules/moment/locale/nb.js",
                "./nb.js": "./node_modules/moment/locale/nb.js",
                "./ne": "./node_modules/moment/locale/ne.js",
                "./ne.js": "./node_modules/moment/locale/ne.js",
                "./nl": "./node_modules/moment/locale/nl.js",
                "./nl-be": "./node_modules/moment/locale/nl-be.js",
                "./nl-be.js": "./node_modules/moment/locale/nl-be.js",
                "./nl.js": "./node_modules/moment/locale/nl.js",
                "./nn": "./node_modules/moment/locale/nn.js",
                "./nn.js": "./node_modules/moment/locale/nn.js",
                "./pa-in": "./node_modules/moment/locale/pa-in.js",
                "./pa-in.js": "./node_modules/moment/locale/pa-in.js",
                "./pl": "./node_modules/moment/locale/pl.js",
                "./pl.js": "./node_modules/moment/locale/pl.js",
                "./pt": "./node_modules/moment/locale/pt.js",
                "./pt-br": "./node_modules/moment/locale/pt-br.js",
                "./pt-br.js": "./node_modules/moment/locale/pt-br.js",
                "./pt.js": "./node_modules/moment/locale/pt.js",
                "./ro": "./node_modules/moment/locale/ro.js",
                "./ro.js": "./node_modules/moment/locale/ro.js",
                "./ru": "./node_modules/moment/locale/ru.js",
                "./ru.js": "./node_modules/moment/locale/ru.js",
                "./sd": "./node_modules/moment/locale/sd.js",
                "./sd.js": "./node_modules/moment/locale/sd.js",
                "./se": "./node_modules/moment/locale/se.js",
                "./se.js": "./node_modules/moment/locale/se.js",
                "./si": "./node_modules/moment/locale/si.js",
                "./si.js": "./node_modules/moment/locale/si.js",
                "./sk": "./node_modules/moment/locale/sk.js",
                "./sk.js": "./node_modules/moment/locale/sk.js",
                "./sl": "./node_modules/moment/locale/sl.js",
                "./sl.js": "./node_modules/moment/locale/sl.js",
                "./sq": "./node_modules/moment/locale/sq.js",
                "./sq.js": "./node_modules/moment/locale/sq.js",
                "./sr": "./node_modules/moment/locale/sr.js",
                "./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
                "./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
                "./sr.js": "./node_modules/moment/locale/sr.js",
                "./ss": "./node_modules/moment/locale/ss.js",
                "./ss.js": "./node_modules/moment/locale/ss.js",
                "./sv": "./node_modules/moment/locale/sv.js",
                "./sv.js": "./node_modules/moment/locale/sv.js",
                "./sw": "./node_modules/moment/locale/sw.js",
                "./sw.js": "./node_modules/moment/locale/sw.js",
                "./ta": "./node_modules/moment/locale/ta.js",
                "./ta.js": "./node_modules/moment/locale/ta.js",
                "./te": "./node_modules/moment/locale/te.js",
                "./te.js": "./node_modules/moment/locale/te.js",
                "./tet": "./node_modules/moment/locale/tet.js",
                "./tet.js": "./node_modules/moment/locale/tet.js",
                "./tg": "./node_modules/moment/locale/tg.js",
                "./tg.js": "./node_modules/moment/locale/tg.js",
                "./th": "./node_modules/moment/locale/th.js",
                "./th.js": "./node_modules/moment/locale/th.js",
                "./tl-ph": "./node_modules/moment/locale/tl-ph.js",
                "./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
                "./tlh": "./node_modules/moment/locale/tlh.js",
                "./tlh.js": "./node_modules/moment/locale/tlh.js",
                "./tr": "./node_modules/moment/locale/tr.js",
                "./tr.js": "./node_modules/moment/locale/tr.js",
                "./tzl": "./node_modules/moment/locale/tzl.js",
                "./tzl.js": "./node_modules/moment/locale/tzl.js",
                "./tzm": "./node_modules/moment/locale/tzm.js",
                "./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
                "./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
                "./tzm.js": "./node_modules/moment/locale/tzm.js",
                "./ug-cn": "./node_modules/moment/locale/ug-cn.js",
                "./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
                "./uk": "./node_modules/moment/locale/uk.js",
                "./uk.js": "./node_modules/moment/locale/uk.js",
                "./ur": "./node_modules/moment/locale/ur.js",
                "./ur.js": "./node_modules/moment/locale/ur.js",
                "./uz": "./node_modules/moment/locale/uz.js",
                "./uz-latn": "./node_modules/moment/locale/uz-latn.js",
                "./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
                "./uz.js": "./node_modules/moment/locale/uz.js",
                "./vi": "./node_modules/moment/locale/vi.js",
                "./vi.js": "./node_modules/moment/locale/vi.js",
                "./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
                "./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
                "./yo": "./node_modules/moment/locale/yo.js",
                "./yo.js": "./node_modules/moment/locale/yo.js",
                "./zh-cn": "./node_modules/moment/locale/zh-cn.js",
                "./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
                "./zh-hk": "./node_modules/moment/locale/zh-hk.js",
                "./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
                "./zh-tw": "./node_modules/moment/locale/zh-tw.js",
                "./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
            };
            function webpackContext(req) {
                var id = webpackContextResolve(req);
                return __webpack_require__(id);
            }
            function webpackContextResolve(req) {
                if (!__webpack_require__.o(map, req)) {
                    var e = new Error("Cannot find module '" + req + "'");
                    e.code = 'MODULE_NOT_FOUND';
                    throw e;
                }
                return map[req];
            }
            webpackContext.keys = function webpackContextKeys() {
                return Object.keys(map);
            };
            webpackContext.resolve = webpackContextResolve;
            module.exports = webpackContext;
            webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/admin/admin-dashboard/admin-dashboard.component.html": 
        /*!************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/admin/admin-dashboard/admin-dashboard.component.html ***!
          \************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div>\n    <div *ngIf='isActivityCreated'> \n    <h3  style=\"color: green;\" > The activity is created successfully! </h3>\n</div>\n\n<mat-card>\n  <mat-card-title>New students</mat-card-title>\n  <mat-card-content><app-student-approve></app-student-approve></mat-card-content>\n</mat-card>\n\n\n<mat-card>\n  <mat-card-title>Activities</mat-card-title>\n  <mat-card-content><app-activities-list></app-activities-list></mat-card-content>\n  <button mat-raised-button color=\"primary\" (click)=\"newActivity()\">New activity</button>\n</mat-card>\n\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/admin/new-activity/new-activity.component.html": 
        /*!******************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/admin/new-activity/new-activity.component.html ***!
          \******************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<form [formGroup]=\"newActivityForm\" (submit)=\"submit(newActivityForm)\">\n<mat-card>\n  <mat-card-title>New activity</mat-card-title>\n  <mat-card-content>\n\n        <div fxLayout=\"row\">\n            <mat-form-field class=\"full-width\">\n              <input matInput placeholder=\"Activity name\" formControlName=\"name\">\n                <mat-error class=\"error-message\" *ngIf=\"fieldHasError('name', 'required')\">\n                  Activity name is required\n                </mat-error>\n            </mat-form-field>\n        </div>\n\n\n       \n        <div fxLayout=\"row\">\n            <mat-form-field class=\"full-width\">\n                  <input matInput placeholder=\"Location\" formControlName=\"location\">\n                    <mat-error class=\"error-message\" *ngIf=\"fieldHasError('location','required')\">\n                      Please fill out the location\n                    </mat-error>\n            </mat-form-field>\n        </div>\n\n\n        <div fxLayout=\"row\">\n            <mat-form-field class=\"full-width\">\n              <textarea matInput placeholder=\"Description\" formControlName=\"description\"></textarea>\n                <mat-error class=\"error-message\" *ngIf=\"fieldHasError('description','required')\">\n                  Please add the description\n                </mat-error>\n            </mat-form-field>\n       </div>\n\n          <div fxLayout=\"row\" fxLayoutGap=\"50px\">\n          <mat-form-field class=\"full-width\">\n            <input matInput placeholder=\"Period of registration\" formControlName=\"periodOfRegistration\">\n                <mat-error class=\"error-message\" *ngIf=\"fieldHasError('periodOfRegistration','required')\">\n                  Period of registration is required\n                </mat-error>\n\n                <mat-error clss=\"error-message\" *ngIf=\"fieldHasError('periodOfRegistration','pattern')\">\n                  Invalid format\n                </mat-error>\n          </mat-form-field>\n\n          <mat-form-field>\n              <input matInput [matDatepicker]=\"picker\" placeholder=\"Choose a date\" formControlName=\"date\">\n              <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n              <mat-datepicker #picker></mat-datepicker>\n              <mat-error class=\"error-message\" *ngIf=\"fieldHasError('date','required')\">\n                  Please ensure the date\n                </mat-error>\n            </mat-form-field>\n          </div>\n\n\n\n\n        <div fxLayout=\"row\">\n          <form>\n            <mat-file-upload [labelText]=\"'Select activity images:'\" [selectButtonText]=\"'Choose File'\" [uploadButtonText]=\"'Upload'\" [allowMultipleFiles]=\"false\" [showUploadButton]=\"true\" [acceptedTypes]=\"'.png, .jpg, .jpeg'\"\n              (uploadClicked)=\"onUploadClicked($event)\"></mat-file-upload>\n            <mat-progress-bar mode=\"determinate\" [value]=\"progress\" *ngIf=\"progress > 0\"></mat-progress-bar>\n          </form>\n        </div>\n\n\n          <div fxLayout=\"row\">\n            <mat-form-field class=\"full-width\">\n              <mat-select placeholder=\"Host\" formControlName=\"host\">\n                <mat-option *ngFor=\"let teacher of teachers\" [value]=\"teacher.email\">\n                  {{ teacher.name + ' ' + teacher.lastName }}\n                </mat-option>\n              </mat-select>\n              <mat-error class=\"error-message\" *ngIf=\"fieldHasError('host','required')\">\n                Please select the host\n              </mat-error>\n            </mat-form-field>\n          </div>\n\n        <mat-error *ngIf=\"unknownError\">Unknown error</mat-error>\n\n  </mat-card-content>\n  <mat-card-actions>\n    <button mat-button class=\"mat-raised-button\"\n      color=\"primary\" type=\"submit\" style=\"margin-left: 5px\" [disabled]=\"!newActivityForm.valid\">Submit</button>\n  </mat-card-actions>\n</mat-card>\n</form>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/admin/student-approve/student-approve.component.html": 
        /*!************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/admin/student-approve/student-approve.component.html ***!
          \************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"mat-elevation-z8\">\n\n  <mat-progress-bar mode=\"indeterminate\" *ngIf=\"dataSource == null\"></mat-progress-bar>\n\n  <mat-table [dataSource]=\"dataSource\" matSort [hidden]=\"dataSource == null\">\n\n    <ng-container matColumnDef=\"image\">\n      <mat-header-cell *matHeaderCellDef><mat-icon [style.padding-left]=\"'10px'\">photo_camera</mat-icon></mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">\n        <div class=\"image-cropper\" *ngIf=\"row.image != null\">\n          <img [src]=\"row.image\" class=\"profile-pic\">\n        </div>\n      </mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"name\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header> Name </mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">\n        <div fxLayout=\"row\">\n          <div>{{row.name}}</div>\n        </div>\n      </mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"lastName\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header> Last name </mat-header-cell>\n      <mat-cell *matCellDef=\"let row\"> {{row.lastName}} </mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"studentId\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header> Student ID </mat-header-cell>\n      <mat-cell *matCellDef=\"let row\"> {{row.studentId}} </mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"email\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header> Email </mat-header-cell>\n      <mat-cell *matCellDef=\"let row\"> {{row.email}} </mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"approve\">\n      <mat-header-cell *matHeaderCellDef >Approve</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">\n        <ng-container *ngIf=\"getStudentProcessingState(row) == null\">\n          <button mat-raised-button color=\"primary\" (click)=\"approve(row)\" type=\"button\">Approve</button>\n        </ng-container>\n        <ng-container style=\"color: goldenrod;\" *ngIf=\"getStudentProcessingState(row) == 'approved'\">\n          Approved!\n        </ng-container>\n        <ng-container style=\"color: grey;\" *ngIf=\"getStudentProcessingState(row) == 'processing'\">\n          Processing...\n        </ng-container>\n      </mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"reject\">\n      <mat-header-cell *matHeaderCellDef >Reject</mat-header-cell>\n      <mat-cell mat-cell *matCellDef=\"let row\">\n        <ng-container *ngIf=\"getStudentProcessingState(row) == null\">\n          <button mat-raised-button color=\"warn\" (click)=\"reject(row)\" type=\"button\">Reject</button>\n        </ng-container>\n        <ng-container style=\"color: red;\"  *ngIf=\"getStudentProcessingState(row) == 'rejected'\">\n          Rejected!\n        </ng-container>\n        <ng-container style=\"color: grey;\" *ngIf=\"getStudentProcessingState(row) == 'processing'\">\n          Processing...\n        </ng-container>\n      </mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"undo\">\n      <mat-header-cell *matHeaderCellDef >Undo</mat-header-cell>\n      <mat-cell mat-cell *matCellDef=\"let row\">\n        <ng-container *ngIf=\"['approved', 'rejected'].indexOf(getStudentProcessingState(row)) != -1\">\n          <button mat-raised-button color=\"primary\" (click)=\"undo(row)\" type=\"button\">Undo</button>\n        </ng-container>\n      </mat-cell>\n    </ng-container>\n\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\">\n    </mat-row>\n  </mat-table>\n\n  <mat-paginator [pageSizeOptions]=\"[5, 10, 50, 100]\"></mat-paginator>\n</div>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html": 
        /*!**************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
          \**************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<app-navigation></app-navigation>\n<router-outlet></router-outlet>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/auth.component.html": 
        /*!********************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/auth/auth.component.html ***!
          \********************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<p>Loading...</p>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/common/activities-list/activities-list.component.html": 
        /*!*************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/common/activities-list/activities-list.component.html ***!
          \*************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"mat-elevation-z8\">\n  <div fxLayout=\"row\" fxLayoutGap=\"50px\">\n      <mat-form-field class=\"full-width\">\n          <input matInput (keyup)=\"applyActivityNameFilter($event.target.value)\" placeholder=\"Filter by activity name\">\n      </mat-form-field>\n\n      <mat-form-field class=\"full-width\">\n          <input matInput #ref (dateChange)=\"applyDateFilter(ref.value)\" (dateInput)=\"applyDateFilter(ref)\" [matDatepicker]=\"picker\" placeholder=\"Filter by date\">\n          <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n          <mat-datepicker #picker></mat-datepicker>\n      </mat-form-field>\n\n  </div>\n  <mat-progress-bar mode=\"indeterminate\" *ngIf=\"dataSource == null\"></mat-progress-bar>\n\n  <mat-table [dataSource]=\"dataSource\" matSort [hidden]=\"dataSource == null\">\n\n    <ng-container matColumnDef=\"name\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header> Activity name </mat-header-cell>\n      <mat-cell *matCellDef=\"let row\"> {{row.name}} </mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"location\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header> Location </mat-header-cell>\n      <mat-cell *matCellDef=\"let row\"> {{row.location}} </mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"periodOfRegistration\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header> Period of registration </mat-header-cell>\n      <mat-cell *matCellDef=\"let row\"> {{row.periodOfRegistration}} </mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"host\">\n        <mat-header-cell *matHeaderCellDef mat-sort-header>  Host  </mat-header-cell>\n        <mat-cell *matCellDef=\"let row\"> {{row.host.name}} {{row.host.lastName}} </mat-cell>\n      </ng-container>\n\n    <ng-container matColumnDef=\"date\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header> Date </mat-header-cell>\n      <mat-cell *matCellDef=\"let row\"> {{formatDate(row.date)}} </mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"enrollment\">\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Enrollment </mat-header-cell>\n        <mat-cell *matCellDef=\"let row\" [style.color]=\"isNoEnrollment(row.activityId) ? 'blue':\n          (isEnrollmentPending(row.activityId) ? '#FFD271':\n          (isEnrollmentApproved(row.activityId) ? 'green' :\n          (isEnrollmentRejected(row.activityId) ? 'red' : null)))\">\n          <ng-container *ngIf=\"isNoEnrollment(row.activityId)\">\n            No enrollment\n          </ng-container>\n          <ng-container *ngIf=\"isEnrollmentPending(row.activityId)\">\n            Enrollment is pending\n          </ng-container>\n          <ng-container *ngIf=\"isEnrollmentApproved(row.activityId)\">\n            Enrollment approved!\n          </ng-container>\n          <ng-container *ngIf=\"isEnrollmentRejected(row.activityId)\">\n            Enrollment rejected\n          </ng-container>\n        </mat-cell>\n      </ng-container>\n\n    <ng-container matColumnDef=\"view\">\n      <mat-header-cell *matHeaderCellDef ></mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">\n        <button mat-raised-button color=\"primary\" (click)=\"viewActivity(row.activityId)\">View</button>\n      </mat-cell>\n    </ng-container>\n\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\">\n    </mat-row>\n  </mat-table>\n\n  <mat-paginator [pageSizeOptions]=\"[5, 10, 50, 100]\"></mat-paginator>\n</div>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/common/activity-comment-list/activity-comment-list.component.html": 
        /*!*************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/common/activity-comment-list/activity-comment-list.component.html ***!
          \*************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<form [formGroup]=\"commentForm\" (ngSubmit)=\"onSubmit(commentText.value)\" *ngIf=\"isStudentOrTeacher\">\n  <mat-card>\n    <mat-card-title>New comment</mat-card-title>\n    <mat-card-content>\n      <mat-form-field class=\"full-width\" class=\"activity-comment-width\">\n        <textarea #commentText matInput placeholder=\"Leave a comment\" formControlName=\"comment\"></textarea>\n        <mat-error class=\"error-message\" *ngIf=\"fieldHasError('comment','required')\">\n            The comment cannot be null\n        </mat-error>\n      </mat-form-field>\n    </mat-card-content>\n    <mat-card-actions>\n      <div class=\"container\">\n        <div class=\"row\">\n          <button mat-raised-button color=\"primary\" type=\"submit\"  [disabled]=\"!commentForm.valid\">Send</button>\n\n        </div>\n      </div>\n    </mat-card-actions>\n  </mat-card>\n</form>\n\n<mat-card *ngFor=\"let comment of comments\">\n\n   <mat-card-title>\n    <div fxLayout=\"row\" fxLayoutGap=\"20px\">\n      <div class=\"image-cropper\" *ngIf=\"getCommenterImage(comment) != null\">\n        <img [src]=\"getCommenterImage(comment)\" class=\"profile-pic\">\n      </div>\n      <div>{{getCommenterName(comment)}}, {{getCommenterType(comment)}}</div>\n    </div>\n  </mat-card-title>\n  <mat-card-subtitle>&nbsp;</mat-card-subtitle>\n  <mat-card-content>\n    {{comment.text}}\n  </mat-card-content>\n  <mat-card-actions *ngIf=\"isAdmin\">\n    <button mat-raised-button color=\"warn\" (click)=\"delete(comment)\" type=\"button\">Delete</button>\n  </mat-card-actions>\n</mat-card>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/common/activity/activity.component.html": 
        /*!***********************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/common/activity/activity.component.html ***!
          \***********************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<mat-progress-bar mode=\"indeterminate\" *ngIf=\"activity == null\"></mat-progress-bar>\n\n<mat-card *ngIf=\"activity != null\">\n  <mat-card-title>{{activity.name}}</mat-card-title>\n  <!-- <img mat-card-lg-image [src]=\"user.image || defaultImageUrl\"> -->\n\n  <mat-card-subtitle>Hosted by {{activity.host.name}} {{activity.host.lastName}}</mat-card-subtitle>\n  <mat-card-content>\n    <div>{{activity.description}}</div>\n    <img *ngIf=\"activity.image != null && activity.image.length > 0\" [attr.src]=\"activity?.image\" [alt]=\"activity?.name\">\n  </mat-card-content>\n  <mat-card-actions *ngIf=\"isStudent\">\n    <ng-container *ngIf=\"isNoEnrollment()\">\n      <button mat-raised-button color=\"primary\" (click)=\"enroll()\">Enroll activity</button>\n    </ng-container>\n    <ng-container *ngIf=\"isEnrollmentPending()\">\n      Enrollment is pending for teacher's approval...\n    </ng-container>\n    <ng-container *ngIf=\"isEnrollmentApproved()\">\n      Enrollment approved!\n    </ng-container>\n    <ng-container *ngIf=\"isEnrollmentRejected()\">\n      Enrollment rejected\n    </ng-container>\n  </mat-card-actions>\n</mat-card>\n\n<mat-card *ngIf=\"activity != null && isTeacher\">\n  <mat-card-title>Enrollments</mat-card-title>\n  <mat-card-content><app-enrollment-approve [activityId]=\"activity.activityId\"></app-enrollment-approve></mat-card-content>\n</mat-card>\n\n<mat-card *ngIf=\"activity != null\">\n  <mat-card-title>Comments</mat-card-title>\n  <mat-card-content><app-activity-comment-list [activity]=\"activity\"></app-activity-comment-list></mat-card-content>\n</mat-card>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/common/update-profile/update-profile.component.html": 
        /*!***********************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/common/update-profile/update-profile.component.html ***!
          \***********************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<form [formGroup]=\"profileUpdateForm\" (submit)=\"submit(profileUpdateForm)\">\n  <mat-card>\n    <mat-card-title>Profile update</mat-card-title>\n    <mat-card-content>\n        <div fxLayout=\"row\" fxLayoutGap=\"50px\">\n          <mat-form-field class=\"full-width\">\n            <input matInput placeholder=\"First name\" formControlName=\"name\">\n\n                 <mat-error class= \"error-message\"\n                  *ngIf = \"fieldHasError('name','required')\">\n                  The name is required\n\n                 </mat-error>\n\n          </mat-form-field>\n\n          <mat-form-field class=\"full-width\">\n            <input matInput placeholder=\"Last name\" formControlName=\"lastname\">\n\n\n                  <mat-error class= \"error-message\"\n                  *ngIf = \"fieldHasError('lastname','required')\">\n                  The last name is required\n                 </mat-error>\n\n          </mat-form-field>\n        </div>\n\n        <div fxLayout=\"row\" fxLayoutGap=\"50px\">\n          <mat-form-field class=\"full-width\">\n            <input matInput placeholder=\"New password\" type=\"password\" formControlName=\"password\">\n              <mat-error class= \"error-message\"\n                *ngIf = \"fieldHasError('password','required')\">\n                Please enter your new password\n              </mat-error>\n          </mat-form-field>\n\n          <mat-form-field class=\"full-width\">\n            <input matInput placeholder=\"Confirm new password\" type=\"password\" formControlName=\"confirmPassword\">\n\n                <mat-error class= \"error-message\"\n                    *ngIf = \"fieldHasError('confirmPassword','mustMatch')\">\n                    Passwords do not match\n                </mat-error>\n\n          </mat-form-field>\n        </div>\n\n        <mat-error *ngIf=\"unknownError\">Unknown error</mat-error>\n    </mat-card-content>\n    <mat-card-actions>\n        <button mat-button class=\"mat-raised-button\" color=\"primary\" type=\"submit\" style=\"margin-left: 5px\"\n        [disabled]=\"!profileUpdateForm.valid\">Update profile</button>\n    </mat-card-actions>\n  </mat-card>\n  </form>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html": 
        /*!**********************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html ***!
          \**********************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"card-center\">\n  <div>\n  <div *ngIf='registerSuccess'> \n  <h3  style=\"color: green;\" > The user is registered successfully! <br/> Please wait for admin to approve</h3>\n  </div>\n\n  \n  <form [formGroup]=\"loginForm\" novalidate (ngSubmit)=\"onSubmit(loginForm)\">\n    <mat-card>        \n\n      <mat-card-title>\n         Welcome to LTAS!\n      </mat-card-title>\n      <mat-card-subtitle>\n        Please enter you email and password to start\n      </mat-card-subtitle>\n      <mat-card-content>\n        <div class=\"container\">\n          <div class=\"row\">\n            <mat-form-field class=\"full-width\">\n              <input matInput placeholder=\"Email\" formControlName=\"email\">\n            </mat-form-field>\n          </div>\n\n          <div class=\"row\">\n            <mat-form-field class=\"full-width\">\n              <input matInput placeholder=\"Password\" type=\"password\" formControlName=\"password\">\n            </mat-form-field>\n          </div>\n        </div>\n\n        <mat-error *ngIf='invalidCredentialsError'> The user is invalid! </mat-error>\n        <mat-error *ngIf='userDisabledError'> The user is disabled,<br/> please wait for admin to approve! </mat-error>\n\n      </mat-card-content>    \n\n      <mat-card-actions>\n        <div class=\"container\">\n          <div class=\"row\">\n            <button mat-raised-button color=\"primary\" type=\"submit\">Sign in</button>\n            <button mat-button (click)=\"studentSignUp()\">Student sign-up</button>\n          </div>\n        </div>\n      </mat-card-actions>\n    </mat-card>\n  </form>\n  </div>\n</div>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/navigation/navigation.component.html": 
        /*!********************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/navigation/navigation.component.html ***!
          \********************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<mat-toolbar color=\"primary\">\n\n  <mat-toolbar-row *ngIf=\"isAuthorized\">\n\n    <button mat-icon-button (click)=\"toDashboard()\">\n      <mat-icon>home</mat-icon>\n      <span *ngIf=\"isAdmin\"> Admin dashboard </span>\n      <span *ngIf=\"isTeacher\"> Teacher dashboard </span>\n      <span *ngIf=\"isStudent\"> Student dashboard </span>\n    </button>\n\n    <div fxFlex fxLayout fxLayoutAlign=\"flex-end\">\n\n      <div fxLayout fxLayoutGap=\"20px\" fxLayoutAlign=\"center center\">\n        <div class=\"image-cropper\" *ngIf=\"person.image != null\">\n          <img [src]=\"person.image\" class=\"profile-pic\">\n        </div>\n\n        <span>{{person.name}} {{person.lastName}}</span>\n\n        <div>\n          <button mat-icon-button [matMenuTriggerFor]=\"auth\">\n            <mat-icon>person_outline</mat-icon>\n          </button>\n          <mat-menu #auth=\"matMenu\">\n            <button mat-menu-item (click)=\"edit()\">\n              <mat-icon>edit</mat-icon>\n              <span>Edit profile</span>\n            </button>\n            <button mat-menu-item (click)=\"logout()\">\n              <mat-icon>exit_to_app</mat-icon>\n              <span>Sign out</span>\n            </button>\n          </mat-menu>\n        </div>\n\n      </div>\n\n    </div>\n\n  </mat-toolbar-row>\n\n</mat-toolbar>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/student/student-dashboard/student-dashboard.component.html": 
        /*!******************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/student/student-dashboard/student-dashboard.component.html ***!
          \******************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<mat-card>\n  <mat-card-title>Activities</mat-card-title>\n  <mat-card-content><app-activities-list></app-activities-list></mat-card-content>\n</mat-card>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/student/student-signup/student-signup.component.html": 
        /*!************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/student/student-signup/student-signup.component.html ***!
          \************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<form [formGroup]=\"studentForm\" (submit)=\"submit(studentForm)\">\n<mat-card>\n  <mat-card-title>Student sign up</mat-card-title>\n  <mat-card-subtitle>Please fill out the form and submit for approval</mat-card-subtitle>\n  <mat-card-content>\n      <div fxLayout=\"row\">\n        <mat-form-field class=\"full-width\">\n          <input matInput placeholder=\"Student Id\" formControlName=\"studentId\">\n\n            <mat-error class=\"error-message\" *ngIf=\"fieldHasError('studentId', 'required')\">\n                Student Id is required\n            </mat-error>\n\n            <mat-error class=\"error-message\" *ngIf=\"fieldHasError('studentId', 'maxlength')\">\n                Student Id is too long\n            </mat-error>\n\n            <mat-error class=\"error-message\" *ngIf=\"fieldHasError('studentId', 'pattern')\">\n                Student Id consists of only number\n            </mat-error>\n\n        </mat-form-field>\n      </div>\n\n      <div fxLayout=\"row\" fxLayoutGap=\"50px\">\n        <mat-form-field class=\"full-width\">\n          <input matInput placeholder=\"First name\" formControlName=\"name\">\n\n               <mat-error class= \"error-message\"\n                *ngIf = \"fieldHasError('name','required')\">\n                The name is required\n\n               </mat-error>\n\n        </mat-form-field>\n\n        <mat-form-field class=\"full-width\">\n          <input matInput placeholder=\"Last name\" formControlName=\"lastname\">\n                <mat-error class= \"error-message\"\n                *ngIf = \"fieldHasError('lastname','required')\">\n                The last name is required\n               </mat-error>\n        </mat-form-field>\n      </div>\n\n      <div fxLayout=\"row\">\n        <form>\n          <mat-file-upload [labelText]=\"'Select your images:'\" [selectButtonText]=\"'Choose File'\" [uploadButtonText]=\"'Submit'\" [allowMultipleFiles]=\"false\" [showUploadButton]=\"true\" [acceptedTypes]=\"'.png, .jpg, .jpeg'\"  (uploadClicked)=\"onUploadClicked($event)\"></mat-file-upload>\n          <mat-progress-bar mode=\"determinate\" [value]=\"progress\" *ngIf=\"progress > 0\"></mat-progress-bar>\n        </form>\n      </div>\n\n      <div fxLayout=\"row\">\n        <mat-form-field class=\"full-width\">\n          <input matInput placeholder=\"Email\" formControlName=\"email\">\n\n\n                <mat-error class= \"error-message\"\n                  *ngIf = \"fieldHasError('email','required')\">\n                  Email address is required\n                </mat-error>\n\n                <mat-error class=\"error-message\"\n                  *ngIf = \"fieldHasError('email', 'email')\">\n                  Email address is invalid\n\n                </mat-error>\n\n\n        </mat-form-field>\n      </div>\n\n      <div fxLayout=\"row\" fxLayoutGap=\"50px\">\n        <mat-form-field class=\"full-width\">\n          <input matInput placeholder=\"Password\" type=\"password\" formControlName=\"password\">\n            <mat-error class= \"error-message\"\n              *ngIf = \"fieldHasError('password','required')\">\n              Please enter your new password\n            </mat-error>\n        </mat-form-field>\n\n        <mat-form-field class=\"full-width\">\n          <input matInput placeholder=\"Confirm Password\" type=\"password\" formControlName=\"confirmPassword\">\n\n              <mat-error class= \"error-message\"\n                  *ngIf = \"fieldHasError('confirmPassword','mustMatch')\">\n                  Passwords do not match\n              </mat-error>\n\n        </mat-form-field>\n      </div>\n\n      <mat-error *ngIf=\"userAlreadyExistsError\">User with this email is already registered</mat-error>\n      <mat-error *ngIf=\"unknownError\">Unknown error</mat-error>\n  </mat-card-content>\n  <mat-card-actions>\n      <button mat-button class=\"mat-raised-button\" color=\"primary\" type=\"submit\" style=\"margin-left: 5px\"\n      [disabled]=\"!studentForm.valid\">Submit for approval</button>\n      <button mat-button class=\"mat-button\" style=\"margin-left: 5px\" routerLink=\"/login\"> Sign in </button>\n  </mat-card-actions>\n</mat-card>\n</form>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/teacher/enrollment-approve/enrollment-approve.component.html": 
        /*!********************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/teacher/enrollment-approve/enrollment-approve.component.html ***!
          \********************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"mat-elevation-z8\">\n\n  <mat-progress-bar mode=\"indeterminate\" *ngIf=\"dataSource == null\"></mat-progress-bar>\n\n  <mat-table [dataSource]=\"dataSource\" matSort *ngIf=\"dataSource != null\">\n\n    <ng-container matColumnDef=\"studentName\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header> Student </mat-header-cell>\n      <mat-cell *matCellDef=\"let row\"> {{row.student.name}} {{row.student.lastName}} </mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"approve\">\n      <mat-header-cell *matHeaderCellDef >Approve</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\" [style.color]=\"'green'\"> \n        <ng-container *ngIf=\"!isProcessing(row) && row.isWaitingForApproval\">\n          <button mat-raised-button color=\"primary\" (click)=\"approve(row)\" type=\"button\">Approve</button>\n        </ng-container>\n        <ng-container *ngIf=\"!isProcessing(row) && !row.isWaitingForApproval && row.isApproved\">\n          Approved!\n        </ng-container>\n        <ng-container *ngIf=\"isProcessing(row)\">\n          Processing...\n        </ng-container>\n      </mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"reject\">\n      <mat-header-cell *matHeaderCellDef >Reject</mat-header-cell>\n      <mat-cell mat-cell *matCellDef=\"let row\" [style.color]=\"'red'\">\n        <ng-container *ngIf=\"!isProcessing(row) && row.isWaitingForApproval\">\n          <button mat-raised-button color=\"warn\" (click)=\"reject(row)\" type=\"button\">Reject</button>\n        </ng-container>\n        <ng-container  *ngIf=\"!isProcessing(row) && !row.isWaitingForApproval && !row.isApproved\">\n          Rejected!\n        </ng-container>\n        <ng-container  *ngIf=\"isProcessing(row)\">\n          Processing...\n        </ng-container>\n      </mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"undo\">\n      <mat-header-cell *matHeaderCellDef >Undo</mat-header-cell>\n      <mat-cell mat-cell *matCellDef=\"let row\" [style.color]=\"'midnightblue'\">\n        <ng-container *ngIf=\"!isProcessing(row) && !row.isWaitingForApproval\">\n          <button mat-raised-button color=\"basic\"  (click)=\"undo(row)\" type=\"button\">Undo</button>\n        </ng-container>\n      </mat-cell>\n    </ng-container>\n\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\">\n    </mat-row>\n  </mat-table>\n\n  <mat-paginator [pageSizeOptions]=\"[5, 10, 50, 100]\"></mat-paginator>\n</div>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/teacher/teacher-dashboard/teacher-dashboard.component.html": 
        /*!******************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/teacher/teacher-dashboard/teacher-dashboard.component.html ***!
          \******************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<mat-card>\n  <mat-card-title>Activities</mat-card-title>\n  <mat-card-content><app-activities-list></app-activities-list></mat-card-content>\n</mat-card>\n");
            /***/ 
        }),
        /***/ "./node_modules/tslib/tslib.es6.js": 
        /*!*****************************************!*\
          !*** ./node_modules/tslib/tslib.es6.js ***!
          \*****************************************/
        /*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function () { return __extends; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function () { return __assign; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function () { return __rest; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function () { return __decorate; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function () { return __param; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function () { return __metadata; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function () { return __awaiter; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function () { return __generator; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function () { return __exportStar; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function () { return __values; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function () { return __read; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function () { return __spread; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function () { return __spreadArrays; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function () { return __await; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function () { return __asyncGenerator; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function () { return __asyncDelegator; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function () { return __asyncValues; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function () { return __makeTemplateObject; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function () { return __importStar; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function () { return __importDefault; });
            /*! *****************************************************************************
            Copyright (c) Microsoft Corporation. All rights reserved.
            Licensed under the Apache License, Version 2.0 (the "License"); you may not use
            this file except in compliance with the License. You may obtain a copy of the
            License at http://www.apache.org/licenses/LICENSE-2.0
            
            THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
            KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
            WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
            MERCHANTABLITY OR NON-INFRINGEMENT.
            
            See the Apache Version 2.0 License for specific language governing permissions
            and limitations under the License.
            ***************************************************************************** */
            /* global Reflect, Promise */
            var extendStatics = function (d, b) {
                extendStatics = Object.setPrototypeOf ||
                    ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
                    function (d, b) { for (var p in b)
                        if (b.hasOwnProperty(p))
                            d[p] = b[p]; };
                return extendStatics(d, b);
            };
            function __extends(d, b) {
                extendStatics(d, b);
                function __() { this.constructor = d; }
                d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
            }
            var __assign = function () {
                __assign = Object.assign || function __assign(t) {
                    for (var s, i = 1, n = arguments.length; i < n; i++) {
                        s = arguments[i];
                        for (var p in s)
                            if (Object.prototype.hasOwnProperty.call(s, p))
                                t[p] = s[p];
                    }
                    return t;
                };
                return __assign.apply(this, arguments);
            };
            function __rest(s, e) {
                var t = {};
                for (var p in s)
                    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
                        t[p] = s[p];
                if (s != null && typeof Object.getOwnPropertySymbols === "function")
                    for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                        if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                            t[p[i]] = s[p[i]];
                    }
                return t;
            }
            function __decorate(decorators, target, key, desc) {
                var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
                    r = Reflect.decorate(decorators, target, key, desc);
                else
                    for (var i = decorators.length - 1; i >= 0; i--)
                        if (d = decorators[i])
                            r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            }
            function __param(paramIndex, decorator) {
                return function (target, key) { decorator(target, key, paramIndex); };
            }
            function __metadata(metadataKey, metadataValue) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function")
                    return Reflect.metadata(metadataKey, metadataValue);
            }
            function __awaiter(thisArg, _arguments, P, generator) {
                return new (P || (P = Promise))(function (resolve, reject) {
                    function fulfilled(value) { try {
                        step(generator.next(value));
                    }
                    catch (e) {
                        reject(e);
                    } }
                    function rejected(value) { try {
                        step(generator["throw"](value));
                    }
                    catch (e) {
                        reject(e);
                    } }
                    function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
                    step((generator = generator.apply(thisArg, _arguments || [])).next());
                });
            }
            function __generator(thisArg, body) {
                var _ = { label: 0, sent: function () { if (t[0] & 1)
                        throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
                return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
                function verb(n) { return function (v) { return step([n, v]); }; }
                function step(op) {
                    if (f)
                        throw new TypeError("Generator is already executing.");
                    while (_)
                        try {
                            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done)
                                return t;
                            if (y = 0, t)
                                op = [op[0] & 2, t.value];
                            switch (op[0]) {
                                case 0:
                                case 1:
                                    t = op;
                                    break;
                                case 4:
                                    _.label++;
                                    return { value: op[1], done: false };
                                case 5:
                                    _.label++;
                                    y = op[1];
                                    op = [0];
                                    continue;
                                case 7:
                                    op = _.ops.pop();
                                    _.trys.pop();
                                    continue;
                                default:
                                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                                        _ = 0;
                                        continue;
                                    }
                                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                                        _.label = op[1];
                                        break;
                                    }
                                    if (op[0] === 6 && _.label < t[1]) {
                                        _.label = t[1];
                                        t = op;
                                        break;
                                    }
                                    if (t && _.label < t[2]) {
                                        _.label = t[2];
                                        _.ops.push(op);
                                        break;
                                    }
                                    if (t[2])
                                        _.ops.pop();
                                    _.trys.pop();
                                    continue;
                            }
                            op = body.call(thisArg, _);
                        }
                        catch (e) {
                            op = [6, e];
                            y = 0;
                        }
                        finally {
                            f = t = 0;
                        }
                    if (op[0] & 5)
                        throw op[1];
                    return { value: op[0] ? op[1] : void 0, done: true };
                }
            }
            function __exportStar(m, exports) {
                for (var p in m)
                    if (!exports.hasOwnProperty(p))
                        exports[p] = m[p];
            }
            function __values(o) {
                var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
                if (m)
                    return m.call(o);
                return {
                    next: function () {
                        if (o && i >= o.length)
                            o = void 0;
                        return { value: o && o[i++], done: !o };
                    }
                };
            }
            function __read(o, n) {
                var m = typeof Symbol === "function" && o[Symbol.iterator];
                if (!m)
                    return o;
                var i = m.call(o), r, ar = [], e;
                try {
                    while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
                        ar.push(r.value);
                }
                catch (error) {
                    e = { error: error };
                }
                finally {
                    try {
                        if (r && !r.done && (m = i["return"]))
                            m.call(i);
                    }
                    finally {
                        if (e)
                            throw e.error;
                    }
                }
                return ar;
            }
            function __spread() {
                for (var ar = [], i = 0; i < arguments.length; i++)
                    ar = ar.concat(__read(arguments[i]));
                return ar;
            }
            function __spreadArrays() {
                for (var s = 0, i = 0, il = arguments.length; i < il; i++)
                    s += arguments[i].length;
                for (var r = Array(s), k = 0, i = 0; i < il; i++)
                    for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                        r[k] = a[j];
                return r;
            }
            ;
            function __await(v) {
                return this instanceof __await ? (this.v = v, this) : new __await(v);
            }
            function __asyncGenerator(thisArg, _arguments, generator) {
                if (!Symbol.asyncIterator)
                    throw new TypeError("Symbol.asyncIterator is not defined.");
                var g = generator.apply(thisArg, _arguments || []), i, q = [];
                return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
                function verb(n) { if (g[n])
                    i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
                function resume(n, v) { try {
                    step(g[n](v));
                }
                catch (e) {
                    settle(q[0][3], e);
                } }
                function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
                function fulfill(value) { resume("next", value); }
                function reject(value) { resume("throw", value); }
                function settle(f, v) { if (f(v), q.shift(), q.length)
                    resume(q[0][0], q[0][1]); }
            }
            function __asyncDelegator(o) {
                var i, p;
                return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
                function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
            }
            function __asyncValues(o) {
                if (!Symbol.asyncIterator)
                    throw new TypeError("Symbol.asyncIterator is not defined.");
                var m = o[Symbol.asyncIterator], i;
                return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
                function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
                function settle(resolve, reject, d, v) { Promise.resolve(v).then(function (v) { resolve({ value: v, done: d }); }, reject); }
            }
            function __makeTemplateObject(cooked, raw) {
                if (Object.defineProperty) {
                    Object.defineProperty(cooked, "raw", { value: raw });
                }
                else {
                    cooked.raw = raw;
                }
                return cooked;
            }
            ;
            function __importStar(mod) {
                if (mod && mod.__esModule)
                    return mod;
                var result = {};
                if (mod != null)
                    for (var k in mod)
                        if (Object.hasOwnProperty.call(mod, k))
                            result[k] = mod[k];
                result.default = mod;
                return result;
            }
            function __importDefault(mod) {
                return (mod && mod.__esModule) ? mod : { default: mod };
            }
            /***/ 
        }),
        /***/ "./src/app/admin/admin-can-activate.ts": 
        /*!*********************************************!*\
          !*** ./src/app/admin/admin-can-activate.ts ***!
          \*********************************************/
        /*! exports provided: AdminCanActivate */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminCanActivate", function () { return AdminCanActivate; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _service_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/authentication.service */ "./src/app/service/authentication.service.ts");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            var AdminCanActivate = /** @class */ (function () {
                function AdminCanActivate(authenticationService, router) {
                    this.authenticationService = authenticationService;
                    this.router = router;
                }
                AdminCanActivate.prototype.canActivate = function (route, state) {
                    var _this = this;
                    var result = null;
                    if (this.authenticationService.isAuthorized) {
                        if (this.authenticationService.isAdmin) {
                            result = true;
                        }
                        else {
                            result = this.router.parseUrl('/authorize');
                        }
                    }
                    else {
                        result = Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["from"])(this.authenticationService.authorize().then(function () {
                            var innerResult = null;
                            if (_this.authenticationService.isAdmin) {
                                innerResult = true;
                            }
                            else {
                                innerResult = _this.router.parseUrl('/authorize');
                            }
                            return innerResult;
                        }));
                    }
                    return result;
                };
                return AdminCanActivate;
            }());
            AdminCanActivate.ctorParameters = function () { return [
                { type: _service_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
            ]; };
            AdminCanActivate = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])()
            ], AdminCanActivate);
            /***/ 
        }),
        /***/ "./src/app/admin/admin-dashboard/admin-dashboard.component.css": 
        /*!*********************************************************************!*\
          !*** ./src/app/admin/admin-dashboard/admin-dashboard.component.css ***!
          \*********************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2FkbWluLWRhc2hib2FyZC9hZG1pbi1kYXNoYm9hcmQuY29tcG9uZW50LmNzcyJ9 */");
            /***/ 
        }),
        /***/ "./src/app/admin/admin-dashboard/admin-dashboard.component.ts": 
        /*!********************************************************************!*\
          !*** ./src/app/admin/admin-dashboard/admin-dashboard.component.ts ***!
          \********************************************************************/
        /*! exports provided: AdminDashboardComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminDashboardComponent", function () { return AdminDashboardComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            var AdminDashboardComponent = /** @class */ (function () {
                function AdminDashboardComponent(router, route) {
                    this.router = router;
                    this.route = route;
                    this.isActivityCreated = false;
                }
                AdminDashboardComponent.prototype.ngOnInit = function () {
                    if (this.route.snapshot.queryParams['cmd'] === 'showCreateActivitySuccess') {
                        this.isActivityCreated = true;
                    }
                };
                AdminDashboardComponent.prototype.newActivity = function () {
                    this.router.navigate(['admin/new-activity']);
                };
                AdminDashboardComponent.prototype.createNewActivity = function () {
                };
                return AdminDashboardComponent;
            }());
            AdminDashboardComponent.ctorParameters = function () { return [
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
            ]; };
            AdminDashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-admin-dashboard',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./admin-dashboard.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/admin/admin-dashboard/admin-dashboard.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./admin-dashboard.component.css */ "./src/app/admin/admin-dashboard/admin-dashboard.component.css")).default]
                })
            ], AdminDashboardComponent);
            /***/ 
        }),
        /***/ "./src/app/admin/admin-routing.module.ts": 
        /*!***********************************************!*\
          !*** ./src/app/admin/admin-routing.module.ts ***!
          \***********************************************/
        /*! exports provided: AdminRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminRoutingModule", function () { return AdminRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _admin_dashboard_admin_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin-dashboard/admin-dashboard.component */ "./src/app/admin/admin-dashboard/admin-dashboard.component.ts");
            /* harmony import */ var _admin_can_activate__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./admin-can-activate */ "./src/app/admin/admin-can-activate.ts");
            /* harmony import */ var _new_activity_new_activity_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./new-activity/new-activity.component */ "./src/app/admin/new-activity/new-activity.component.ts");
            var routes = [
                { path: 'admin/dashboard', component: _admin_dashboard_admin_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["AdminDashboardComponent"], canActivate: [_admin_can_activate__WEBPACK_IMPORTED_MODULE_4__["AdminCanActivate"]] },
                { path: 'admin/new-activity', component: _new_activity_new_activity_component__WEBPACK_IMPORTED_MODULE_5__["NewActivityComponent"], canActivate: [_admin_can_activate__WEBPACK_IMPORTED_MODULE_4__["AdminCanActivate"]] }
            ];
            var AdminRoutingModule = /** @class */ (function () {
                function AdminRoutingModule() {
                }
                return AdminRoutingModule;
            }());
            AdminRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
                    providers: [_admin_can_activate__WEBPACK_IMPORTED_MODULE_4__["AdminCanActivate"]]
                })
            ], AdminRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/admin/new-activity/new-activity.component.css": 
        /*!***************************************************************!*\
          !*** ./src/app/admin/new-activity/new-activity.component.css ***!
          \***************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".full-width {\n    width: 300px;\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vbmV3LWFjdGl2aXR5L25ldy1hY3Rpdml0eS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksWUFBWTtFQUNkIiwiZmlsZSI6InNyYy9hcHAvYWRtaW4vbmV3LWFjdGl2aXR5L25ldy1hY3Rpdml0eS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZ1bGwtd2lkdGgge1xuICAgIHdpZHRoOiAzMDBweDtcbiAgfSJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/admin/new-activity/new-activity.component.ts": 
        /*!**************************************************************!*\
          !*** ./src/app/admin/new-activity/new-activity.component.ts ***!
          \**************************************************************/
        /*! exports provided: NewActivityComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewActivityComponent", function () { return NewActivityComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
            /* harmony import */ var src_app_service_teacher_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/teacher.service */ "./src/app/service/teacher.service.ts");
            /* harmony import */ var src_app_service_activity_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/service/activity.service */ "./src/app/service/activity.service.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var src_app_service_file_upload_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/service/file-upload.service */ "./src/app/service/file-upload.service.ts");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            var NewActivityComponent = /** @class */ (function () {
                function NewActivityComponent(formBuilder, teacherService, activityService, fileUploadService, router) {
                    this.formBuilder = formBuilder;
                    this.teacherService = teacherService;
                    this.activityService = activityService;
                    this.fileUploadService = fileUploadService;
                    this.router = router;
                    this.isActivityCreated = false;
                    this.newActivityForm = this.formBuilder.group({
                        name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                        location: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                        description: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                        periodOfRegistration: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('[0-9]+')])],
                        date: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                        host: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                        image: ['']
                    });
                    window.router = this.router;
                }
                NewActivityComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.teacherService.getAll().then(function (teachers) {
                        _this.teachers = teachers;
                    });
                };
                NewActivityComponent.prototype.fieldHasError = function (fieldName, errorType) {
                    return this.newActivityForm.get(fieldName).hasError(errorType) &&
                        (this.newActivityForm.get(fieldName).dirty || this.newActivityForm.get(fieldName).touched);
                };
                NewActivityComponent.prototype.submit = function (_a) {
                    var value = _a.value, valid = _a.valid;
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                        var activity;
                        var _this = this;
                        return __generator(this, function (_a) {
                            activity = {
                                activityId: null,
                                name: value.name,
                                location: value.location,
                                description: value.description,
                                periodOfRegistration: value.periodOfRegistration,
                                date: value.date,
                                image: value.image,
                                host: this.teachers.find(function (teacher) { return teacher.email === value.host; })
                            };
                            this.activityService.newActivity(activity).then(function () {
                                _this.router.navigate(['/admin/dashboard'], { queryParams: { cmd: 'showCreateActivitySuccess' } });
                            }, function (err) {
                                _this.unknownError = true;
                            });
                            return [2 /*return*/];
                        });
                    });
                };
                NewActivityComponent.prototype.onUploadClicked = function (files) {
                    var _this = this;
                    console.log(typeof (files));
                    console.log(files.item(0));
                    var uploadedFile = files.item(0);
                    this.progress = 0;
                    this.fileUploadService.uploadFile(uploadedFile)
                        .subscribe(function (event) {
                        switch (event.type) {
                            case _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpEventType"].Sent:
                                console.log('Request has been made!');
                                break;
                            case _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpEventType"].ResponseHeader:
                                console.log('Response header has been received!');
                                break;
                            case _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpEventType"].UploadProgress:
                                _this.progress = Math.round(event.loaded / event.total * 100);
                                console.log("Uploaded! " + _this.progress + "%");
                                break;
                            case _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpEventType"].Response:
                                console.log('User successfully created!', event.body);
                                _this.uploadedUrl = event.body;
                                _this.newActivityForm.patchValue({
                                    image: _this.uploadedUrl
                                });
                                _this.newActivityForm.get('image').updateValueAndValidity();
                                setTimeout(function () {
                                    _this.progress = 0;
                                }, 1500);
                        }
                    });
                };
                NewActivityComponent.prototype.onSelectedFilesChanged = function (files) {
                };
                return NewActivityComponent;
            }());
            NewActivityComponent.ctorParameters = function () { return [
                { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
                { type: src_app_service_teacher_service__WEBPACK_IMPORTED_MODULE_3__["TeacherService"] },
                { type: src_app_service_activity_service__WEBPACK_IMPORTED_MODULE_4__["ActivityService"] },
                { type: src_app_service_file_upload_service__WEBPACK_IMPORTED_MODULE_6__["FileUploadService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
            ]; };
            NewActivityComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-new-activity',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./new-activity.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/admin/new-activity/new-activity.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./new-activity.component.css */ "./src/app/admin/new-activity/new-activity.component.css")).default]
                })
            ], NewActivityComponent);
            /***/ 
        }),
        /***/ "./src/app/admin/student-approve/student-approve.component.css": 
        /*!*********************************************************************!*\
          !*** ./src/app/admin/student-approve/student-approve.component.css ***!
          \*********************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".mat-column-image {\n  word-wrap: break-word !important;\n  white-space: unset !important;\n  flex: 0 0 60px !important;\n  width: 60px !important;\n  overflow-wrap: break-word;\n  word-wrap: break-word;\n\n  word-break: break-word;\n\n  -ms-hyphens: auto;\n  -webkit-hyphens: auto;\n  hyphens: auto;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vc3R1ZGVudC1hcHByb3ZlL3N0dWRlbnQtYXBwcm92ZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0NBQWdDO0VBQ2hDLDZCQUE2QjtFQUM3Qix5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLHlCQUF5QjtFQUN6QixxQkFBcUI7O0VBRXJCLHNCQUFzQjs7RUFFdEIsaUJBQWlCO0VBRWpCLHFCQUFxQjtFQUNyQixhQUFhO0FBQ2YiLCJmaWxlIjoic3JjL2FwcC9hZG1pbi9zdHVkZW50LWFwcHJvdmUvc3R1ZGVudC1hcHByb3ZlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWF0LWNvbHVtbi1pbWFnZSB7XG4gIHdvcmQtd3JhcDogYnJlYWstd29yZCAhaW1wb3J0YW50O1xuICB3aGl0ZS1zcGFjZTogdW5zZXQgIWltcG9ydGFudDtcbiAgZmxleDogMCAwIDYwcHggIWltcG9ydGFudDtcbiAgd2lkdGg6IDYwcHggIWltcG9ydGFudDtcbiAgb3ZlcmZsb3ctd3JhcDogYnJlYWstd29yZDtcbiAgd29yZC13cmFwOiBicmVhay13b3JkO1xuXG4gIHdvcmQtYnJlYWs6IGJyZWFrLXdvcmQ7XG5cbiAgLW1zLWh5cGhlbnM6IGF1dG87XG4gIC1tb3otaHlwaGVuczogYXV0bztcbiAgLXdlYmtpdC1oeXBoZW5zOiBhdXRvO1xuICBoeXBoZW5zOiBhdXRvO1xufVxuIl19 */");
            /***/ 
        }),
        /***/ "./src/app/admin/student-approve/student-approve.component.ts": 
        /*!********************************************************************!*\
          !*** ./src/app/admin/student-approve/student-approve.component.ts ***!
          \********************************************************************/
        /*! exports provided: StudentApproveComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentApproveComponent", function () { return StudentApproveComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
            /* harmony import */ var src_app_service_student_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/student.service */ "./src/app/service/student.service.ts");
            var StudentApproveComponent = /** @class */ (function () {
                function StudentApproveComponent(studentService) {
                    this.studentService = studentService;
                    this.displayedColumns = ['image', 'studentId', 'email', 'name', 'lastName', 'approve', 'reject', 'undo'];
                    this.studentProcessingStates = new Map();
                }
                StudentApproveComponent.prototype.getStudentProcessingState = function (student) {
                    return this.studentProcessingStates.get(student) || null;
                };
                StudentApproveComponent.prototype.approve = function (student) {
                    var _this = this;
                    this.studentProcessingStates.set(student, 'processing');
                    this.studentService.approve(student.email).then(function () {
                        _this.studentProcessingStates.set(student, 'approved');
                    }, function () {
                        _this.studentProcessingStates.set(student, null);
                    });
                };
                StudentApproveComponent.prototype.reject = function (student) {
                    var _this = this;
                    this.studentProcessingStates.set(student, 'processing');
                    this.studentService.reject(student.email).then(function () {
                        _this.studentProcessingStates.set(student, 'rejected');
                    }, function () {
                        _this.studentProcessingStates.set(student, null);
                    });
                };
                StudentApproveComponent.prototype.undo = function (student) {
                    this.studentProcessingStates.set(student, null);
                };
                StudentApproveComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.studentService.getAllWaitingForApprovalStudents().then(function (students) {
                        _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](students);
                        _this.dataSource.paginator = _this.paginator;
                        _this.dataSource.sort = _this.sort;
                    });
                };
                return StudentApproveComponent;
            }());
            StudentApproveComponent.ctorParameters = function () { return [
                { type: src_app_service_student_service__WEBPACK_IMPORTED_MODULE_3__["StudentService"] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"], null)
            ], StudentApproveComponent.prototype, "paginator", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"], null)
            ], StudentApproveComponent.prototype, "sort", void 0);
            StudentApproveComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-student-approve',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./student-approve.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/admin/student-approve/student-approve.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./student-approve.component.css */ "./src/app/admin/student-approve/student-approve.component.css")).default]
                })
            ], StudentApproveComponent);
            /***/ 
        }),
        /***/ "./src/app/app-routing.module.ts": 
        /*!***************************************!*\
          !*** ./src/app/app-routing.module.ts ***!
          \***************************************/
        /*! exports provided: AppRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () { return AppRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
            /* harmony import */ var _auth_auth_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./auth/auth.component */ "./src/app/auth/auth.component.ts");
            var routes = [
                {
                    path: '',
                    redirectTo: 'authorize',
                    pathMatch: 'full'
                },
                { path: 'authorize', component: _auth_auth_component__WEBPACK_IMPORTED_MODULE_4__["AuthComponent"] },
                { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
                { path: '**', redirectTo: 'authorize' }
            ];
            var AppRoutingModule = /** @class */ (function () {
                function AppRoutingModule() {
                }
                return AppRoutingModule;
            }());
            AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], AppRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/app.component.css": 
        /*!***********************************!*\
          !*** ./src/app/app.component.css ***!
          \***********************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */");
            /***/ 
        }),
        /***/ "./src/app/app.component.ts": 
        /*!**********************************!*\
          !*** ./src/app/app.component.ts ***!
          \**********************************/
        /*! exports provided: AppComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function () { return AppComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var AppComponent = /** @class */ (function () {
                function AppComponent() {
                }
                return AppComponent;
            }());
            AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-root',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")).default]
                })
            ], AppComponent);
            /***/ 
        }),
        /***/ "./src/app/app.module.ts": 
        /*!*******************************!*\
          !*** ./src/app/app.module.ts ***!
          \*******************************/
        /*! exports provided: AppModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function () { return AppModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
            /* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
            /* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm2015/card.js");
            /* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
            /* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
            /* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
            /* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
            /* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
            /* harmony import */ var _navigation_navigation_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./navigation/navigation.component */ "./src/app/navigation/navigation.component.ts");
            /* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm2015/form-field.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            /* harmony import */ var _admin_admin_dashboard_admin_dashboard_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./admin/admin-dashboard/admin-dashboard.component */ "./src/app/admin/admin-dashboard/admin-dashboard.component.ts");
            /* harmony import */ var _teacher_teacher_dashboard_teacher_dashboard_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./teacher/teacher-dashboard/teacher-dashboard.component */ "./src/app/teacher/teacher-dashboard/teacher-dashboard.component.ts");
            /* harmony import */ var _student_student_dashboard_student_dashboard_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./student/student-dashboard/student-dashboard.component */ "./src/app/student/student-dashboard/student-dashboard.component.ts");
            /* harmony import */ var _auth_auth_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./auth/auth.component */ "./src/app/auth/auth.component.ts");
            /* harmony import */ var _service_jwt_interceptor_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./service/jwt-interceptor.service */ "./src/app/service/jwt-interceptor.service.ts");
            /* harmony import */ var _student_student_signup_student_signup_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./student/student-signup/student-signup.component */ "./src/app/student/student-signup/student-signup.component.ts");
            /* harmony import */ var _admin_student_approve_student_approve_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./admin/student-approve/student-approve.component */ "./src/app/admin/student-approve/student-approve.component.ts");
            /* harmony import */ var _admin_admin_routing_module__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./admin/admin-routing.module */ "./src/app/admin/admin-routing.module.ts");
            /* harmony import */ var _student_student_routing_module__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./student/student-routing.module */ "./src/app/student/student-routing.module.ts");
            /* harmony import */ var _teacher_teacher_routing_module__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./teacher/teacher-routing.module */ "./src/app/teacher/teacher-routing.module.ts");
            /* harmony import */ var _admin_new_activity_new_activity_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./admin/new-activity/new-activity.component */ "./src/app/admin/new-activity/new-activity.component.ts");
            /* harmony import */ var _common_activities_list_activities_list_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./common/activities-list/activities-list.component */ "./src/app/common/activities-list/activities-list.component.ts");
            /* harmony import */ var _common_common_routing_module__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./common/common-routing.module */ "./src/app/common/common-routing.module.ts");
            /* harmony import */ var _common_activity_activity_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./common/activity/activity.component */ "./src/app/common/activity/activity.component.ts");
            /* harmony import */ var _teacher_enrollment_approve_enrollment_approve_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./teacher/enrollment-approve/enrollment-approve.component */ "./src/app/teacher/enrollment-approve/enrollment-approve.component.ts");
            /* harmony import */ var _common_activity_comment_list_activity_comment_list_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./common/activity-comment-list/activity-comment-list.component */ "./src/app/common/activity-comment-list/activity-comment-list.component.ts");
            /* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm2015/datepicker.js");
            /* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm2015/core.js");
            /* harmony import */ var _common_update_profile_update_profile_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./common/update-profile/update-profile.component */ "./src/app/common/update-profile/update-profile.component.ts");
            /* harmony import */ var mat_file_upload__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! mat-file-upload */ "./node_modules/mat-file-upload/fesm2015/mat-file-upload.js");
            var AppModule = /** @class */ (function () {
                function AppModule() {
                }
                return AppModule;
            }());
            AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
                    declarations: [
                        _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"],
                        _login_login_component__WEBPACK_IMPORTED_MODULE_10__["LoginComponent"],
                        _navigation_navigation_component__WEBPACK_IMPORTED_MODULE_11__["NavigationComponent"],
                        _teacher_teacher_dashboard_teacher_dashboard_component__WEBPACK_IMPORTED_MODULE_15__["TeacherDashboardComponent"],
                        _student_student_dashboard_student_dashboard_component__WEBPACK_IMPORTED_MODULE_16__["StudentDashboardComponent"],
                        _auth_auth_component__WEBPACK_IMPORTED_MODULE_17__["AuthComponent"],
                        _student_student_signup_student_signup_component__WEBPACK_IMPORTED_MODULE_19__["StudentSignupComponent"],
                        _admin_admin_dashboard_admin_dashboard_component__WEBPACK_IMPORTED_MODULE_14__["AdminDashboardComponent"],
                        _admin_student_approve_student_approve_component__WEBPACK_IMPORTED_MODULE_20__["StudentApproveComponent"],
                        _admin_new_activity_new_activity_component__WEBPACK_IMPORTED_MODULE_24__["NewActivityComponent"],
                        _teacher_enrollment_approve_enrollment_approve_component__WEBPACK_IMPORTED_MODULE_28__["EnrollmentApproveComponent"],
                        _common_activities_list_activities_list_component__WEBPACK_IMPORTED_MODULE_25__["ActivitiesListComponent"],
                        _common_activity_activity_component__WEBPACK_IMPORTED_MODULE_27__["ActivityComponent"],
                        _common_activity_comment_list_activity_comment_list_component__WEBPACK_IMPORTED_MODULE_29__["ActivityCommentListComponent"],
                        _common_update_profile_update_profile_component__WEBPACK_IMPORTED_MODULE_32__["UpdateProfileComponent"]
                    ],
                    imports: [
                        _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                        _admin_admin_routing_module__WEBPACK_IMPORTED_MODULE_21__["AdminRoutingModule"],
                        _student_student_routing_module__WEBPACK_IMPORTED_MODULE_22__["StudentRoutingModule"],
                        _teacher_teacher_routing_module__WEBPACK_IMPORTED_MODULE_23__["TeacherRoutingModule"],
                        _common_common_routing_module__WEBPACK_IMPORTED_MODULE_26__["CommonRoutingModule"],
                        _app_routing_module__WEBPACK_IMPORTED_MODULE_7__["AppRoutingModule"],
                        _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_9__["BrowserAnimationsModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSidenavModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatToolbarModule"],
                        _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__["FlexLayoutModule"],
                        _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                        _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                        _angular_material_card__WEBPACK_IMPORTED_MODULE_5__["MatCardModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatMenuModule"],
                        _angular_material_form_field__WEBPACK_IMPORTED_MODULE_12__["MatFormFieldModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginatorModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatProgressBarModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatProgressSpinnerModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"],
                        _angular_common_http__WEBPACK_IMPORTED_MODULE_13__["HttpClientModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSelectModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatOptionModule"],
                        mat_file_upload__WEBPACK_IMPORTED_MODULE_33__["MatFileUploadModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                        _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_30__["MatDatepickerModule"],
                        _angular_material_core__WEBPACK_IMPORTED_MODULE_31__["MatNativeDateModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSortModule"]
                    ],
                    providers: [
                        { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_13__["HTTP_INTERCEPTORS"], useClass: _service_jwt_interceptor_service__WEBPACK_IMPORTED_MODULE_18__["JwtInterceptorService"], multi: true },
                    ],
                    bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]],
                    entryComponents: [_admin_new_activity_new_activity_component__WEBPACK_IMPORTED_MODULE_24__["NewActivityComponent"]]
                })
            ], AppModule);
            /***/ 
        }),
        /***/ "./src/app/auth/auth.component.css": 
        /*!*****************************************!*\
          !*** ./src/app/auth/auth.component.css ***!
          \*****************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvYXV0aC5jb21wb25lbnQuY3NzIn0= */");
            /***/ 
        }),
        /***/ "./src/app/auth/auth.component.ts": 
        /*!****************************************!*\
          !*** ./src/app/auth/auth.component.ts ***!
          \****************************************/
        /*! exports provided: AuthComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthComponent", function () { return AuthComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _service_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/authentication.service */ "./src/app/service/authentication.service.ts");
            /* harmony import */ var _service_dashboard_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/dashboard.service */ "./src/app/service/dashboard.service.ts");
            var AuthComponent = /** @class */ (function () {
                function AuthComponent(authenticationService, dashboardService) {
                    this.authenticationService = authenticationService;
                    this.dashboardService = dashboardService;
                }
                AuthComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    if (this.authenticationService.authToken != null) {
                        this.authenticationService.authorize().catch(function (err) { return null; }).then(function () {
                            _this.dashboardService.toDashboard();
                        });
                    }
                    else {
                        this.dashboardService.toDashboard();
                    }
                };
                return AuthComponent;
            }());
            AuthComponent.ctorParameters = function () { return [
                { type: _service_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"] },
                { type: _service_dashboard_service__WEBPACK_IMPORTED_MODULE_3__["DashboardService"] }
            ]; };
            AuthComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-auth',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./auth.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/auth.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./auth.component.css */ "./src/app/auth/auth.component.css")).default]
                })
            ], AuthComponent);
            /***/ 
        }),
        /***/ "./src/app/common/activities-list/activities-list.component.css": 
        /*!**********************************************************************!*\
          !*** ./src/app/common/activities-list/activities-list.component.css ***!
          \**********************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbW1vbi9hY3Rpdml0aWVzLWxpc3QvYWN0aXZpdGllcy1saXN0LmNvbXBvbmVudC5jc3MifQ== */");
            /***/ 
        }),
        /***/ "./src/app/common/activities-list/activities-list.component.ts": 
        /*!*********************************************************************!*\
          !*** ./src/app/common/activities-list/activities-list.component.ts ***!
          \*********************************************************************/
        /*! exports provided: ActivitiesListComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivitiesListComponent", function () { return ActivitiesListComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
            /* harmony import */ var src_app_service_activity_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/activity.service */ "./src/app/service/activity.service.ts");
            /* harmony import */ var src_app_service_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/service/authentication.service */ "./src/app/service/authentication.service.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var src_app_service_enrollment_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/service/enrollment.service */ "./src/app/service/enrollment.service.ts");
            /* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
            /* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/ __webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);
            var ActivitiesListComponent = /** @class */ (function () {
                function ActivitiesListComponent(authService, activityService, enrollmentService, router) {
                    this.authService = authService;
                    this.activityService = activityService;
                    this.enrollmentService = enrollmentService;
                    this.router = router;
                    this.nameFilter = '';
                    this.dateFilter = '';
                    this.displayedColumns = ['name', 'location', 'periodOfRegistration', 'date'].
                        concat(!authService.isTeacher ? ['host'] : []).
                        concat(authService.isStudent ? ['enrollment'] : []).
                        concat(['view']);
                }
                ActivitiesListComponent.prototype.ngOnInit = function () {
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                        var _a, _b, _c;
                        return __generator(this, function (_d) {
                            switch (_d.label) {
                                case 0:
                                    if (!this.authService.isTeacher) return [3 /*break*/, 2];
                                    _a = this;
                                    return [4 /*yield*/, this.activityService.getAllActivitiesForTeacher()];
                                case 1:
                                    _a.activities = _d.sent();
                                    return [3 /*break*/, 5];
                                case 2:
                                    _b = this;
                                    return [4 /*yield*/, this.activityService.getAll()];
                                case 3:
                                    _b.activities = _d.sent();
                                    if (!this.authService.isStudent) return [3 /*break*/, 5];
                                    _c = this;
                                    return [4 /*yield*/, this.enrollmentService.getAllOwnEnrollments()];
                                case 4:
                                    _c.enrollments = _d.sent();
                                    _d.label = 5;
                                case 5:
                                    this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](this.activities);
                                    this.dataSource.paginator = this.paginator;
                                    this.dataSource.sort = this.sort;
                                    this.dataSource.filterPredicate = function (data, filter) {
                                        var result = true;
                                        if (filter.indexOf("$$$$") !== -1) {
                                            var _a = __read(filter.split("$$$$"), 2), nameFilter = _a[0], dateFilter = _a[1];
                                            if (nameFilter.length > 0) {
                                                result = result && (data.name != null && data.name.indexOf(nameFilter) != -1);
                                            }
                                            if (dateFilter.length > 0) {
                                                result = result && data.date != null && new Date(data.date).getTime() === new Date(dateFilter).getTime();
                                            }
                                        }
                                        return result;
                                    };
                                    return [2 /*return*/];
                            }
                        });
                    });
                };
                ActivitiesListComponent.prototype.viewActivity = function (activityId) {
                    this.router.navigate(["/common/activity/" + activityId]);
                };
                ActivitiesListComponent.prototype.applyActivityNameFilter = function (filterValue) {
                    filterValue = filterValue.trim();
                    filterValue = filterValue.toLowerCase();
                    this.nameFilter = filterValue;
                    this.dataSource.filter = this.nameFilter + "$$$$" + this.dateFilter;
                };
                ActivitiesListComponent.prototype.applyDateFilter = function (filterValue) {
                    this.dateFilter = filterValue;
                    this.dataSource.filter = this.nameFilter + "$$$$" + this.dateFilter;
                };
                ActivitiesListComponent.prototype.getEnrollmentByActivityId = function (activityId) {
                    return this.enrollments.find(function (enrollment) { return enrollment.activity.activityId == activityId; });
                };
                ActivitiesListComponent.prototype.isEnrollmentApproved = function (activityId) {
                    var enrollment = this.getEnrollmentByActivityId(activityId);
                    return enrollment != null && !enrollment.isWaitingForApproval && enrollment.isApproved;
                };
                ActivitiesListComponent.prototype.isEnrollmentRejected = function (activityId) {
                    var enrollment = this.getEnrollmentByActivityId(activityId);
                    return enrollment != null && !enrollment.isWaitingForApproval && !enrollment.isApproved;
                };
                ActivitiesListComponent.prototype.isEnrollmentPending = function (activityId) {
                    var enrollment = this.getEnrollmentByActivityId(activityId);
                    return enrollment != null && enrollment.isWaitingForApproval;
                };
                ActivitiesListComponent.prototype.isNoEnrollment = function (activityId) {
                    var enrollment = this.getEnrollmentByActivityId(activityId);
                    return enrollment == null;
                };
                ActivitiesListComponent.prototype.formatDate = function (dateStr) {
                    return dateStr != null ? moment__WEBPACK_IMPORTED_MODULE_7__(new Date(dateStr)).format('MMMM Do YYYY') : '';
                };
                return ActivitiesListComponent;
            }());
            ActivitiesListComponent.ctorParameters = function () { return [
                { type: src_app_service_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] },
                { type: src_app_service_activity_service__WEBPACK_IMPORTED_MODULE_3__["ActivityService"] },
                { type: src_app_service_enrollment_service__WEBPACK_IMPORTED_MODULE_6__["EnrollmentService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"], null)
            ], ActivitiesListComponent.prototype, "paginator", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"], null)
            ], ActivitiesListComponent.prototype, "sort", void 0);
            ActivitiesListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-activities-list',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./activities-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/common/activities-list/activities-list.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./activities-list.component.css */ "./src/app/common/activities-list/activities-list.component.css")).default]
                })
            ], ActivitiesListComponent);
            /***/ 
        }),
        /***/ "./src/app/common/activity-comment-list/activity-comment-list.component.css": 
        /*!**********************************************************************************!*\
          !*** ./src/app/common/activity-comment-list/activity-comment-list.component.css ***!
          \**********************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".activity-comment-width {\n  min-width: 300px;\n  max-width: 600px;\n  width: 100%;\n}\nimg {\n  border-radius: 50%;\n\n}\n.mat-card-lg-image {\n  width: 40px;\n  height: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tbW9uL2FjdGl2aXR5LWNvbW1lbnQtbGlzdC9hY3Rpdml0eS1jb21tZW50LWxpc3QuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsV0FBVztBQUNiO0FBQ0E7RUFDRSxrQkFBa0I7O0FBRXBCO0FBQ0E7RUFDRSxXQUFXO0VBQ1gsWUFBWTtBQUNkIiwiZmlsZSI6InNyYy9hcHAvY29tbW9uL2FjdGl2aXR5LWNvbW1lbnQtbGlzdC9hY3Rpdml0eS1jb21tZW50LWxpc3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hY3Rpdml0eS1jb21tZW50LXdpZHRoIHtcbiAgbWluLXdpZHRoOiAzMDBweDtcbiAgbWF4LXdpZHRoOiA2MDBweDtcbiAgd2lkdGg6IDEwMCU7XG59XG5pbWcge1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG5cbn1cbi5tYXQtY2FyZC1sZy1pbWFnZSB7XG4gIHdpZHRoOiA0MHB4O1xuICBoZWlnaHQ6IDQwcHg7XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/common/activity-comment-list/activity-comment-list.component.ts": 
        /*!*********************************************************************************!*\
          !*** ./src/app/common/activity-comment-list/activity-comment-list.component.ts ***!
          \*********************************************************************************/
        /*! exports provided: ActivityCommentListComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivityCommentListComponent", function () { return ActivityCommentListComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var src_app_service_activity_comment_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/activity-comment.service */ "./src/app/service/activity-comment.service.ts");
            /* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
            /* harmony import */ var src_app_service_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/service/authentication.service */ "./src/app/service/authentication.service.ts");
            var ActivityCommentListComponent = /** @class */ (function () {
                function ActivityCommentListComponent(activityCommentService, formBuilder, authService) {
                    this.activityCommentService = activityCommentService;
                    this.formBuilder = formBuilder;
                    this.authService = authService;
                    this.commentForm = this.formBuilder.group({
                        comment: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
                    });
                }
                Object.defineProperty(ActivityCommentListComponent.prototype, "isAdmin", {
                    get: function () {
                        return this.authService.isAdmin;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ActivityCommentListComponent.prototype, "isStudentOrTeacher", {
                    get: function () {
                        return this.authService.isStudent || this.authService.isTeacher;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ActivityCommentListComponent.prototype, "activity", {
                    get: function () {
                        return this._activity;
                    },
                    set: function (value) {
                        this._activity = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                ActivityCommentListComponent.prototype.ngOnInit = function () {
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                        var _a;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = this;
                                    return [4 /*yield*/, this.activityCommentService.getComments(this.activity.activityId)];
                                case 1:
                                    _a.comments = _b.sent();
                                    return [2 /*return*/];
                            }
                        });
                    });
                };
                ActivityCommentListComponent.prototype.onSubmit = function (commentText) {
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                        var newComment, key;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, this.activityCommentService.newActivityComment(this.activity, commentText)];
                                case 1:
                                    newComment = _a.sent();
                                    this.commentForm.reset();
                                    for (key in this.commentForm.controls) {
                                        this.commentForm.get(key).setErrors(null);
                                    }
                                    this.comments = [newComment].concat(this.comments);
                                    return [2 /*return*/];
                            }
                        });
                    });
                };
                ActivityCommentListComponent.prototype.delete = function (comment) {
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, this.activityCommentService.deleteActivityComment(comment.commentId)];
                                case 1:
                                    _a.sent();
                                    this.comments = this.comments.filter(function (_comment) { return _comment !== comment; });
                                    return [2 /*return*/];
                            }
                        });
                    });
                };
                ActivityCommentListComponent.prototype.getCommenterImage = function (comment) {
                    var result = null;
                    if (comment.postedByAdmin) {
                        result = comment.postedByAdmin.image;
                    }
                    else if (comment.postedByTeacher) {
                        result = comment.postedByTeacher.image;
                    }
                    else if (comment.postedByStudent) {
                        result = comment.postedByStudent.image;
                    }
                    return result;
                };
                ActivityCommentListComponent.prototype.getCommenterName = function (comment) {
                    var result = null;
                    if (comment.postedByAdmin) {
                        result = comment.postedByAdmin.name + " " + comment.postedByAdmin.lastName;
                    }
                    else if (comment.postedByTeacher) {
                        result = comment.postedByTeacher.name + " " + comment.postedByTeacher.lastName;
                    }
                    else if (comment.postedByStudent) {
                        result = comment.postedByStudent.name + " " + comment.postedByStudent.lastName;
                    }
                    return result;
                };
                ActivityCommentListComponent.prototype.getCommenterType = function (comment) {
                    var result = null;
                    if (comment.postedByAdmin) {
                        result = "Admin";
                    }
                    else if (comment.postedByTeacher) {
                        result = "Teacher";
                    }
                    else if (comment.postedByStudent) {
                        result = "Student";
                    }
                    return result;
                };
                ActivityCommentListComponent.prototype.fieldHasError = function (fieldName, errorType) {
                    return this.commentForm.get(fieldName).hasError(errorType) &&
                        (this.commentForm.get(fieldName).dirty || this.commentForm.get(fieldName).touched);
                };
                return ActivityCommentListComponent;
            }());
            ActivityCommentListComponent.ctorParameters = function () { return [
                { type: src_app_service_activity_comment_service__WEBPACK_IMPORTED_MODULE_2__["ActivityCommentService"] },
                { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
                { type: src_app_service_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
            ], ActivityCommentListComponent.prototype, "activity", null);
            ActivityCommentListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-activity-comment-list',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./activity-comment-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/common/activity-comment-list/activity-comment-list.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./activity-comment-list.component.css */ "./src/app/common/activity-comment-list/activity-comment-list.component.css")).default]
                })
            ], ActivityCommentListComponent);
            /***/ 
        }),
        /***/ "./src/app/common/activity/activity.component.css": 
        /*!********************************************************!*\
          !*** ./src/app/common/activity/activity.component.css ***!
          \********************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbW1vbi9hY3Rpdml0eS9hY3Rpdml0eS5jb21wb25lbnQuY3NzIn0= */");
            /***/ 
        }),
        /***/ "./src/app/common/activity/activity.component.ts": 
        /*!*******************************************************!*\
          !*** ./src/app/common/activity/activity.component.ts ***!
          \*******************************************************/
        /*! exports provided: ActivityComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivityComponent", function () { return ActivityComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var src_app_service_activity_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/activity.service */ "./src/app/service/activity.service.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var src_app_service_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/service/authentication.service */ "./src/app/service/authentication.service.ts");
            /* harmony import */ var src_app_service_enrollment_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/service/enrollment.service */ "./src/app/service/enrollment.service.ts");
            var ActivityComponent = /** @class */ (function () {
                function ActivityComponent(activityService, authService, enrollmentService, route) {
                    this.activityService = activityService;
                    this.authService = authService;
                    this.enrollmentService = enrollmentService;
                    this.route = route;
                }
                ActivityComponent.prototype.ngOnInit = function () {
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                        var activityId, _a, _b;
                        return __generator(this, function (_c) {
                            switch (_c.label) {
                                case 0:
                                    activityId = this.route.snapshot.params['activityId'];
                                    _a = this;
                                    return [4 /*yield*/, this.activityService.getActivity(activityId)];
                                case 1:
                                    _a.activity = _c.sent();
                                    if (!this.isStudent) return [3 /*break*/, 3];
                                    _b = this;
                                    return [4 /*yield*/, this.enrollmentService.getStudentEnrollment(activityId)];
                                case 2:
                                    _b.enrollment = _c.sent();
                                    _c.label = 3;
                                case 3: return [2 /*return*/];
                            }
                        });
                    });
                };
                Object.defineProperty(ActivityComponent.prototype, "isStudent", {
                    get: function () {
                        return this.authService.isStudent;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ActivityComponent.prototype, "isTeacher", {
                    get: function () {
                        return this.authService.isTeacher;
                    },
                    enumerable: true,
                    configurable: true
                });
                ActivityComponent.prototype.enroll = function () {
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                        var _a;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = this;
                                    return [4 /*yield*/, this.enrollmentService.enroll(this.activity.activityId)];
                                case 1:
                                    _a.enrollment = _b.sent();
                                    return [2 /*return*/];
                            }
                        });
                    });
                };
                ActivityComponent.prototype.isEnrollmentApproved = function () {
                    return this.enrollment != null && !this.enrollment.isWaitingForApproval && this.enrollment.isApproved;
                };
                ActivityComponent.prototype.isEnrollmentRejected = function () {
                    return this.enrollment != null && !this.enrollment.isWaitingForApproval && !this.enrollment.isApproved;
                };
                ActivityComponent.prototype.isEnrollmentPending = function () {
                    return this.enrollment != null && this.enrollment.isWaitingForApproval;
                };
                ActivityComponent.prototype.isNoEnrollment = function () {
                    return this.enrollment == null;
                };
                return ActivityComponent;
            }());
            ActivityComponent.ctorParameters = function () { return [
                { type: src_app_service_activity_service__WEBPACK_IMPORTED_MODULE_2__["ActivityService"] },
                { type: src_app_service_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] },
                { type: src_app_service_enrollment_service__WEBPACK_IMPORTED_MODULE_5__["EnrollmentService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
            ]; };
            ActivityComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-activity',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./activity.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/common/activity/activity.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./activity.component.css */ "./src/app/common/activity/activity.component.css")).default]
                })
            ], ActivityComponent);
            /***/ 
        }),
        /***/ "./src/app/common/any-can-activate.ts": 
        /*!********************************************!*\
          !*** ./src/app/common/any-can-activate.ts ***!
          \********************************************/
        /*! exports provided: AnyCanActivate */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnyCanActivate", function () { return AnyCanActivate; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _service_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/authentication.service */ "./src/app/service/authentication.service.ts");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            var AnyCanActivate = /** @class */ (function () {
                function AnyCanActivate(authenticationService, router) {
                    this.authenticationService = authenticationService;
                    this.router = router;
                }
                AnyCanActivate.prototype.canActivate = function (route, state) {
                    var result = null;
                    if (this.authenticationService.isAuthorized) {
                        result = true;
                    }
                    else {
                        result = Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["from"])(this.authenticationService.authorize().then(function () { return true; }));
                    }
                    return result;
                };
                return AnyCanActivate;
            }());
            AnyCanActivate.ctorParameters = function () { return [
                { type: _service_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
            ]; };
            AnyCanActivate = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])()
            ], AnyCanActivate);
            /***/ 
        }),
        /***/ "./src/app/common/common-routing.module.ts": 
        /*!*************************************************!*\
          !*** ./src/app/common/common-routing.module.ts ***!
          \*************************************************/
        /*! exports provided: CommonRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonRoutingModule", function () { return CommonRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _activity_activity_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./activity/activity.component */ "./src/app/common/activity/activity.component.ts");
            /* harmony import */ var _any_can_activate__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./any-can-activate */ "./src/app/common/any-can-activate.ts");
            /* harmony import */ var _update_profile_update_profile_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./update-profile/update-profile.component */ "./src/app/common/update-profile/update-profile.component.ts");
            var routes = [
                { path: 'common/activity/:activityId', component: _activity_activity_component__WEBPACK_IMPORTED_MODULE_3__["ActivityComponent"], canActivate: [_any_can_activate__WEBPACK_IMPORTED_MODULE_4__["AnyCanActivate"]] },
                { path: 'common/updateProfile', component: _update_profile_update_profile_component__WEBPACK_IMPORTED_MODULE_5__["UpdateProfileComponent"], canActivate: [_any_can_activate__WEBPACK_IMPORTED_MODULE_4__["AnyCanActivate"]] },
            ];
            var CommonRoutingModule = /** @class */ (function () {
                function CommonRoutingModule() {
                }
                return CommonRoutingModule;
            }());
            CommonRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
                    providers: [_any_can_activate__WEBPACK_IMPORTED_MODULE_4__["AnyCanActivate"]]
                })
            ], CommonRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/common/update-profile/update-profile.component.css": 
        /*!********************************************************************!*\
          !*** ./src/app/common/update-profile/update-profile.component.css ***!
          \********************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbW1vbi91cGRhdGUtcHJvZmlsZS91cGRhdGUtcHJvZmlsZS5jb21wb25lbnQuY3NzIn0= */");
            /***/ 
        }),
        /***/ "./src/app/common/update-profile/update-profile.component.ts": 
        /*!*******************************************************************!*\
          !*** ./src/app/common/update-profile/update-profile.component.ts ***!
          \*******************************************************************/
        /*! exports provided: UpdateProfileComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateProfileComponent", function () { return UpdateProfileComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var src_app_service_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/authentication.service */ "./src/app/service/authentication.service.ts");
            /* harmony import */ var src_app_service_person_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/service/person.service */ "./src/app/service/person.service.ts");
            /* harmony import */ var src_app_service_dashboard_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/service/dashboard.service */ "./src/app/service/dashboard.service.ts");
            var UpdateProfileComponent = /** @class */ (function () {
                function UpdateProfileComponent(formBuilder, personService, authService, dashboardService) {
                    this.formBuilder = formBuilder;
                    this.personService = personService;
                    this.authService = authService;
                    this.dashboardService = dashboardService;
                    this.unknownError = false;
                    this.profileUpdateForm = this.formBuilder.group({
                        name: [authService.curPerson.name, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                        lastname: [authService.curPerson.lastName, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                        password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                        confirmPassword: ['']
                    }, { validator: this.checkPasswords });
                }
                UpdateProfileComponent.prototype.checkPasswords = function (group) {
                    var pass = group.get('password');
                    var confirmPass = group.get('confirmPassword');
                    if (pass.value !== confirmPass.value) {
                        confirmPass.setErrors({ mustMatch: true });
                    }
                    else {
                        confirmPass.setErrors(null);
                    }
                };
                UpdateProfileComponent.prototype.ngOnInit = function () {
                };
                UpdateProfileComponent.prototype.fieldHasError = function (fieldName, errorType) {
                    return this.profileUpdateForm.get(fieldName).hasError(errorType) &&
                        (this.profileUpdateForm.get(fieldName).dirty || this.profileUpdateForm.get(fieldName).touched);
                };
                UpdateProfileComponent.prototype.submit = function (_a) {
                    var _this = this;
                    var value = _a.value, valid = _a.valid;
                    var password = value.password;
                    var person = {
                        email: this.authService.curPerson.email,
                        name: value.name,
                        lastName: value.lastName,
                        image: null
                    };
                    this.personService.updateProfile(person, password).then(function () {
                        _this.dashboardService.toDashboard();
                    }, function (err) {
                        _this.unknownError = true;
                    });
                };
                return UpdateProfileComponent;
            }());
            UpdateProfileComponent.ctorParameters = function () { return [
                { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] },
                { type: src_app_service_person_service__WEBPACK_IMPORTED_MODULE_4__["PersonService"] },
                { type: src_app_service_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] },
                { type: src_app_service_dashboard_service__WEBPACK_IMPORTED_MODULE_5__["DashboardService"] }
            ]; };
            UpdateProfileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
                    selector: 'app-update-profile',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./update-profile.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/common/update-profile/update-profile.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./update-profile.component.css */ "./src/app/common/update-profile/update-profile.component.css")).default]
                })
            ], UpdateProfileComponent);
            /***/ 
        }),
        /***/ "./src/app/login/login.component.css": 
        /*!*******************************************!*\
          !*** ./src/app/login/login.component.css ***!
          \*******************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".card-center {\n  height: 100vh;\n  display: flex;\n  justify-content: center;\n  padding-top: 100px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQWE7RUFDYixhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLGtCQUFrQjtBQUNwQiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FyZC1jZW50ZXIge1xuICBoZWlnaHQ6IDEwMHZoO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgcGFkZGluZy10b3A6IDEwMHB4O1xufSJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/login/login.component.ts": 
        /*!******************************************!*\
          !*** ./src/app/login/login.component.ts ***!
          \******************************************/
        /*! exports provided: LoginComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function () { return LoginComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _service_dashboard_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../service/dashboard.service */ "./src/app/service/dashboard.service.ts");
            /* harmony import */ var _service_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/authentication.service */ "./src/app/service/authentication.service.ts");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            var LoginComponent = /** @class */ (function () {
                function LoginComponent(fb, router, route, dashboardService, authenticationService) {
                    this.fb = fb;
                    this.router = router;
                    this.route = route;
                    this.dashboardService = dashboardService;
                    this.authenticationService = authenticationService;
                    this.loginForm = this.fb.group({
                        email: [''],
                        password: ['']
                    });
                    this.registerSuccess = false;
                }
                LoginComponent.prototype.ngOnInit = function () {
                    if (this.route.snapshot.queryParams['cmd'] === 'showSignupSuccess') {
                        this.registerSuccess = true;
                    }
                };
                LoginComponent.prototype.onSubmit = function (_a) {
                    var value = _a.value, valid = _a.valid;
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                        var err_1;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    _a.trys.push([0, 2, , 3]);
                                    return [4 /*yield*/, this.authenticationService.login(value.email, value.password)];
                                case 1:
                                    _a.sent();
                                    this.dashboardService.toDashboard();
                                    return [3 /*break*/, 3];
                                case 2:
                                    err_1 = _a.sent();
                                    switch (err_1.message) {
                                        case 'Invalid credentials':
                                            this.invalidCredentialsError = true;
                                            this.userDisabledError = false;
                                            break;
                                        case 'User disabled':
                                            this.invalidCredentialsError = false;
                                            this.userDisabledError = true;
                                            break;
                                    }
                                    return [3 /*break*/, 3];
                                case 3: return [2 /*return*/];
                            }
                        });
                    });
                };
                LoginComponent.prototype.studentSignUp = function () {
                    this.router.navigate(['student/signup']);
                };
                LoginComponent.prototype.signup = function () {
                };
                return LoginComponent;
            }());
            LoginComponent.ctorParameters = function () { return [
                { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
                { type: _service_dashboard_service__WEBPACK_IMPORTED_MODULE_1__["DashboardService"] },
                { type: _service_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"] }
            ]; };
            LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
                    selector: 'app-login',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")).default]
                })
            ], LoginComponent);
            /***/ 
        }),
        /***/ "./src/app/navigation/navigation.component.css": 
        /*!*****************************************************!*\
          !*** ./src/app/navigation/navigation.component.css ***!
          \*****************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25hdmlnYXRpb24vbmF2aWdhdGlvbi5jb21wb25lbnQuY3NzIn0= */");
            /***/ 
        }),
        /***/ "./src/app/navigation/navigation.component.ts": 
        /*!****************************************************!*\
          !*** ./src/app/navigation/navigation.component.ts ***!
          \****************************************************/
        /*! exports provided: NavigationComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationComponent", function () { return NavigationComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _service_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/authentication.service */ "./src/app/service/authentication.service.ts");
            /* harmony import */ var _service_dashboard_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/dashboard.service */ "./src/app/service/dashboard.service.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            var NavigationComponent = /** @class */ (function () {
                function NavigationComponent(authService, dashboardService, router) {
                    this.authService = authService;
                    this.dashboardService = dashboardService;
                    this.router = router;
                }
                Object.defineProperty(NavigationComponent.prototype, "isAuthorized", {
                    get: function () {
                        return this.authService.curPerson != null;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(NavigationComponent.prototype, "isAdmin", {
                    get: function () {
                        return this.authService.isAdmin;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(NavigationComponent.prototype, "isTeacher", {
                    get: function () {
                        return this.authService.isTeacher;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(NavigationComponent.prototype, "isStudent", {
                    get: function () {
                        return this.authService.isStudent;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(NavigationComponent.prototype, "person", {
                    get: function () {
                        return this.authService.curPerson;
                    },
                    enumerable: true,
                    configurable: true
                });
                NavigationComponent.prototype.toDashboard = function () {
                    this.dashboardService.toDashboard();
                };
                NavigationComponent.prototype.logout = function () {
                    this.authService.logout();
                    this.dashboardService.toDashboard();
                };
                NavigationComponent.prototype.edit = function () {
                    this.router.navigate(['/common/updateProfile']);
                };
                NavigationComponent.prototype.ngOnInit = function () {
                };
                return NavigationComponent;
            }());
            NavigationComponent.ctorParameters = function () { return [
                { type: _service_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"] },
                { type: _service_dashboard_service__WEBPACK_IMPORTED_MODULE_3__["DashboardService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
            ]; };
            NavigationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-navigation',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./navigation.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/navigation/navigation.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./navigation.component.css */ "./src/app/navigation/navigation.component.css")).default]
                })
            ], NavigationComponent);
            /***/ 
        }),
        /***/ "./src/app/service/activity-comment.service.ts": 
        /*!*****************************************************!*\
          !*** ./src/app/service/activity-comment.service.ts ***!
          \*****************************************************/
        /*! exports provided: ActivityCommentService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivityCommentService", function () { return ActivityCommentService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            var ActivityCommentService = /** @class */ (function () {
                function ActivityCommentService(http) {
                    this.http = http;
                }
                ActivityCommentService.prototype.newActivityComment = function (activity, text) {
                    var activityComment = {
                        commentId: null,
                        activity: activity,
                        postedByStudent: null,
                        postedByTeacher: null,
                        postedByAdmin: null,
                        text: text,
                        postedOn: null
                    };
                    return this.http.post('/api/studentAndTeacher/activityComments', activityComment).toPromise();
                };
                ActivityCommentService.prototype.getComments = function (activityId) {
                    return this.http.get("/api/public/activityComments/" + activityId).toPromise();
                };
                ActivityCommentService.prototype.deleteActivityComment = function (activityCommentId) {
                    return this.http.delete("/api/admin/activityComments/" + activityCommentId).toPromise();
                };
                return ActivityCommentService;
            }());
            ActivityCommentService.ctorParameters = function () { return [
                { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
            ]; };
            ActivityCommentService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], ActivityCommentService);
            /***/ 
        }),
        /***/ "./src/app/service/activity.service.ts": 
        /*!*********************************************!*\
          !*** ./src/app/service/activity.service.ts ***!
          \*********************************************/
        /*! exports provided: ActivityService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivityService", function () { return ActivityService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            var ActivityService = /** @class */ (function () {
                function ActivityService(http) {
                    this.http = http;
                }
                ActivityService.prototype.newActivity = function (activity) {
                    return this.http.post('/api/admin/activities/new', activity).toPromise();
                };
                ActivityService.prototype.getAll = function () {
                    return this.http.get('/api/public/activities').toPromise();
                };
                ActivityService.prototype.getAllActivitiesForTeacher = function () {
                    return this.http.get('/api/teacher/activities/getAllOwn').toPromise();
                };
                ActivityService.prototype.getAllActivities = function () {
                    return this.http.get('/api/studentAndTeacher/activities').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) { return result; })).toPromise();
                };
                ActivityService.prototype.getActivity = function (activityId) {
                    return this.http.get("/api/public/activities/getActivity/" + activityId).toPromise();
                };
                return ActivityService;
            }());
            ActivityService.ctorParameters = function () { return [
                { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
            ]; };
            ActivityService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], ActivityService);
            /***/ 
        }),
        /***/ "./src/app/service/authentication.service.ts": 
        /*!***************************************************!*\
          !*** ./src/app/service/authentication.service.ts ***!
          \***************************************************/
        /*! exports provided: AuthenticationService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function () { return AuthenticationService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            var AuthenticationService = /** @class */ (function () {
                function AuthenticationService(http) {
                    this.http = http;
                    this._isAuthorized = false;
                }
                Object.defineProperty(AuthenticationService.prototype, "curPerson", {
                    get: function () {
                        return this._curPerson;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(AuthenticationService.prototype, "curUserType", {
                    get: function () {
                        return this._curUserType;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(AuthenticationService.prototype, "authToken", {
                    get: function () {
                        return localStorage.getItem('authToken');
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(AuthenticationService.prototype, "isAuthorized", {
                    get: function () {
                        return this._isAuthorized;
                    },
                    enumerable: true,
                    configurable: true
                });
                AuthenticationService.prototype.saveAuthToken = function (authToken) {
                    localStorage.setItem('authToken', authToken);
                };
                AuthenticationService.prototype.resetAuthToken = function () {
                    localStorage.removeItem('authToken');
                };
                Object.defineProperty(AuthenticationService.prototype, "isAdmin", {
                    get: function () {
                        return this._curUserType === 'ADMIN';
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(AuthenticationService.prototype, "isStudent", {
                    get: function () {
                        return this._curUserType === 'STUDENT';
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(AuthenticationService.prototype, "isTeacher", {
                    get: function () {
                        return this._curUserType === 'TEACHER';
                    },
                    enumerable: true,
                    configurable: true
                });
                AuthenticationService.prototype.authorize = function () {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        _this.http.get('/api/public/authorize').
                            subscribe(function (data) {
                            if (data.userType) {
                                _this._isAuthorized = true;
                                _this._curPerson = data.person;
                                _this._curUserType = data.userType;
                                resolve();
                            }
                            else {
                                reject(new Error('No data'));
                            }
                        }, function (err) {
                            reject(new Error(err.error));
                        });
                    });
                };
                AuthenticationService.prototype.login = function (email, password) {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        _this.resetAuthToken();
                        _this.http.post('/api/public/auth', {
                            email: email,
                            password: password
                        }).subscribe(function (data) {
                            if (data.token) {
                                _this.saveAuthToken(data.token);
                                _this._isAuthorized = true;
                                _this._curPerson = data.person;
                                _this._curUserType = data.userType;
                                resolve();
                            }
                            else {
                                reject(new Error('No data'));
                            }
                        }, function (err) {
                            reject(new Error(err.error.message));
                        });
                    });
                };
                AuthenticationService.prototype.logout = function () {
                    this.resetAuthToken();
                    this._isAuthorized = false;
                    this._curPerson = null;
                    this._curUserType = null;
                };
                return AuthenticationService;
            }());
            AuthenticationService.ctorParameters = function () { return [
                { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
            ]; };
            AuthenticationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], AuthenticationService);
            /***/ 
        }),
        /***/ "./src/app/service/dashboard.service.ts": 
        /*!**********************************************!*\
          !*** ./src/app/service/dashboard.service.ts ***!
          \**********************************************/
        /*! exports provided: DashboardService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardService", function () { return DashboardService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./authentication.service */ "./src/app/service/authentication.service.ts");
            var DashboardService = /** @class */ (function () {
                function DashboardService(router, authenticationService) {
                    this.router = router;
                    this.authenticationService = authenticationService;
                }
                DashboardService.prototype.toDashboard = function () {
                    if (this.authenticationService.isAdmin) {
                        this.router.navigate(['/admin/dashboard']);
                    }
                    else if (this.authenticationService.isStudent) {
                        this.router.navigate(['/student/dashboard']);
                    }
                    else if (this.authenticationService.isTeacher) {
                        this.router.navigate(['/teacher/dashboard']);
                    }
                    else {
                        this.router.navigate(['/login']);
                    }
                };
                return DashboardService;
            }());
            DashboardService.ctorParameters = function () { return [
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
                { type: _authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] }
            ]; };
            DashboardService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], DashboardService);
            /***/ 
        }),
        /***/ "./src/app/service/enrollment.service.ts": 
        /*!***********************************************!*\
          !*** ./src/app/service/enrollment.service.ts ***!
          \***********************************************/
        /*! exports provided: EnrollmentService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnrollmentService", function () { return EnrollmentService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            var EnrollmentService = /** @class */ (function () {
                function EnrollmentService(http) {
                    this.http = http;
                }
                EnrollmentService.prototype.getStudentEnrollment = function (activityId) {
                    return this.http.get("/api/student/enrollments/get/" + activityId).toPromise();
                };
                EnrollmentService.prototype.enroll = function (activityId) {
                    return this.http.get("/api/student/enrollments/enroll/" + activityId).toPromise();
                };
                EnrollmentService.prototype.getAllOwnEnrollments = function () {
                    return this.http.get('/api/student/enrollments').toPromise();
                };
                EnrollmentService.prototype.getActivityEnrollments = function (activityId) {
                    return this.http.get("/api/teacher/enrollments/activity/" + activityId).toPromise();
                };
                EnrollmentService.prototype.approveEnrollment = function (enrollmentId) {
                    return this.http.get("/api/teacher/enrollments/approve/" + enrollmentId).toPromise();
                };
                EnrollmentService.prototype.rejectEnrollment = function (enrollmentId) {
                    return this.http.get("/api/teacher/enrollments/reject/" + enrollmentId).toPromise();
                };
                return EnrollmentService;
            }());
            EnrollmentService.ctorParameters = function () { return [
                { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
            ]; };
            EnrollmentService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], EnrollmentService);
            /***/ 
        }),
        /***/ "./src/app/service/file-upload.service.ts": 
        /*!************************************************!*\
          !*** ./src/app/service/file-upload.service.ts ***!
          \************************************************/
        /*! exports provided: FileUploadService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileUploadService", function () { return FileUploadService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            var FileUploadService = /** @class */ (function () {
                function FileUploadService(http) {
                    this.http = http;
                }
                FileUploadService.prototype.uploadFile = function (image) {
                    var formData = new FormData();
                    formData.append('file', image);
                    return this.http.post('/api/public/uploadFile', formData, {
                        reportProgress: true,
                        observe: 'events',
                        responseType: 'text'
                    }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorMgmt));
                };
                FileUploadService.prototype.errorMgmt = function (error) {
                    var errorMessage = '';
                    if (error.error instanceof ErrorEvent) {
                        // Get client-side error
                        errorMessage = error.error.message;
                    }
                    else {
                        // Get server-side error
                        errorMessage = "Error Code: " + error.status + "\nMessage: " + error.message;
                    }
                    console.log(errorMessage);
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(errorMessage);
                };
                return FileUploadService;
            }());
            FileUploadService.ctorParameters = function () { return [
                { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
            ]; };
            FileUploadService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Injectable"])({
                    providedIn: 'root'
                })
            ], FileUploadService);
            /***/ 
        }),
        /***/ "./src/app/service/jwt-interceptor.service.ts": 
        /*!****************************************************!*\
          !*** ./src/app/service/jwt-interceptor.service.ts ***!
          \****************************************************/
        /*! exports provided: JwtInterceptorService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptorService", function () { return JwtInterceptorService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _authentication_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./authentication.service */ "./src/app/service/authentication.service.ts");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var JwtInterceptorService = /** @class */ (function () {
                function JwtInterceptorService(authenticationService) {
                    this.authenticationService = authenticationService;
                }
                JwtInterceptorService.prototype.intercept = function (request, next) {
                    var token = this.authenticationService.authToken;
                    if (token) {
                        request = request.clone({
                            setHeaders: {
                                Authorization: "Bearer " + token
                            }
                        });
                    }
                    return next.handle(request);
                };
                return JwtInterceptorService;
            }());
            JwtInterceptorService.ctorParameters = function () { return [
                { type: _authentication_service__WEBPACK_IMPORTED_MODULE_1__["AuthenticationService"] }
            ]; };
            JwtInterceptorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
                    providedIn: 'root'
                })
            ], JwtInterceptorService);
            /***/ 
        }),
        /***/ "./src/app/service/person.service.ts": 
        /*!*******************************************!*\
          !*** ./src/app/service/person.service.ts ***!
          \*******************************************/
        /*! exports provided: PersonService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PersonService", function () { return PersonService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            var PersonService = /** @class */ (function () {
                function PersonService(http) {
                    this.http = http;
                }
                PersonService.prototype.updateProfile = function (person, password) {
                    return this.http.post('/api/public/persons/update', {
                        person: person,
                        password: password
                    }).toPromise();
                };
                return PersonService;
            }());
            PersonService.ctorParameters = function () { return [
                { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
            ]; };
            PersonService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], PersonService);
            /***/ 
        }),
        /***/ "./src/app/service/student.service.ts": 
        /*!********************************************!*\
          !*** ./src/app/service/student.service.ts ***!
          \********************************************/
        /*! exports provided: StudentService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentService", function () { return StudentService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            var StudentService = /** @class */ (function () {
                function StudentService(http) {
                    this.http = http;
                }
                StudentService.prototype.signup = function (student, password) {
                    return this.http.post('/api/public/students/register', {
                        student: student,
                        password: password
                    }).toPromise();
                };
                StudentService.prototype.getAllWaitingForApprovalStudents = function () {
                    return this.http.get('/api/admin/students/loadWaitingForApproval').toPromise();
                };
                StudentService.prototype.approve = function (email) {
                    return this.http.post('/api/admin/students/approve', email).toPromise();
                };
                StudentService.prototype.reject = function (email) {
                    return this.http.post('/api/admin/students/reject', email).toPromise();
                };
                return StudentService;
            }());
            StudentService.ctorParameters = function () { return [
                { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
            ]; };
            StudentService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], StudentService);
            /***/ 
        }),
        /***/ "./src/app/service/teacher.service.ts": 
        /*!********************************************!*\
          !*** ./src/app/service/teacher.service.ts ***!
          \********************************************/
        /*! exports provided: TeacherService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeacherService", function () { return TeacherService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            var TeacherService = /** @class */ (function () {
                function TeacherService(http) {
                    this.http = http;
                }
                TeacherService.prototype.getAll = function () {
                    return this.http.get('/api/admin/teachers').toPromise();
                };
                return TeacherService;
            }());
            TeacherService.ctorParameters = function () { return [
                { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
            ]; };
            TeacherService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], TeacherService);
            /***/ 
        }),
        /***/ "./src/app/student/student-can-activate.ts": 
        /*!*************************************************!*\
          !*** ./src/app/student/student-can-activate.ts ***!
          \*************************************************/
        /*! exports provided: StudentCanActivate */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentCanActivate", function () { return StudentCanActivate; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _service_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/authentication.service */ "./src/app/service/authentication.service.ts");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            var StudentCanActivate = /** @class */ (function () {
                function StudentCanActivate(authenticationService, router) {
                    this.authenticationService = authenticationService;
                    this.router = router;
                }
                StudentCanActivate.prototype.canActivate = function (route, state) {
                    var _this = this;
                    var result = null;
                    if (this.authenticationService.isAuthorized) {
                        if (this.authenticationService.isStudent) {
                            result = true;
                        }
                        else {
                            result = this.router.parseUrl('/authorize');
                        }
                    }
                    else {
                        result = Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["from"])(this.authenticationService.authorize().then(function () {
                            var innerResult = null;
                            if (_this.authenticationService.isStudent) {
                                innerResult = true;
                            }
                            else {
                                innerResult = _this.router.parseUrl('/authorize');
                            }
                            return innerResult;
                        }));
                    }
                    return result;
                };
                return StudentCanActivate;
            }());
            StudentCanActivate.ctorParameters = function () { return [
                { type: _service_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
            ]; };
            StudentCanActivate = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])()
            ], StudentCanActivate);
            /***/ 
        }),
        /***/ "./src/app/student/student-dashboard/student-dashboard.component.css": 
        /*!***************************************************************************!*\
          !*** ./src/app/student/student-dashboard/student-dashboard.component.css ***!
          \***************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("/* Absolute Center Spinner */\n.loading-indicator {\n    position: fixed;\n    z-index: 999;\n    height: 2em;\n    width: 2em;\n    overflow: show;\n    margin: auto;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    right: 0;\n}\n/* Transparent Overlay */\n.loading-indicator:before {\n    content: '';\n    display: block;\n    position: fixed;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    background-color: rgba(0,0,0,0.3);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZGVudC9zdHVkZW50LWRhc2hib2FyZC9zdHVkZW50LWRhc2hib2FyZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDRCQUE0QjtBQUM1QjtJQUNJLGVBQWU7SUFDZixZQUFZO0lBQ1osV0FBVztJQUNYLFVBQVU7SUFDVixjQUFjO0lBQ2QsWUFBWTtJQUNaLE1BQU07SUFDTixPQUFPO0lBQ1AsU0FBUztJQUNULFFBQVE7QUFDWjtBQUVBLHdCQUF3QjtBQUN4QjtJQUNJLFdBQVc7SUFDWCxjQUFjO0lBQ2QsZUFBZTtJQUNmLE1BQU07SUFDTixPQUFPO0lBQ1AsV0FBVztJQUNYLFlBQVk7SUFDWixpQ0FBaUM7QUFDckMiLCJmaWxlIjoic3JjL2FwcC9zdHVkZW50L3N0dWRlbnQtZGFzaGJvYXJkL3N0dWRlbnQtZGFzaGJvYXJkLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBBYnNvbHV0ZSBDZW50ZXIgU3Bpbm5lciAqL1xuLmxvYWRpbmctaW5kaWNhdG9yIHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgei1pbmRleDogOTk5O1xuICAgIGhlaWdodDogMmVtO1xuICAgIHdpZHRoOiAyZW07XG4gICAgb3ZlcmZsb3c6IHNob3c7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIHRvcDogMDtcbiAgICBsZWZ0OiAwO1xuICAgIGJvdHRvbTogMDtcbiAgICByaWdodDogMDtcbn1cblxuLyogVHJhbnNwYXJlbnQgT3ZlcmxheSAqL1xuLmxvYWRpbmctaW5kaWNhdG9yOmJlZm9yZSB7XG4gICAgY29udGVudDogJyc7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHRvcDogMDtcbiAgICBsZWZ0OiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuMyk7XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/student/student-dashboard/student-dashboard.component.ts": 
        /*!**************************************************************************!*\
          !*** ./src/app/student/student-dashboard/student-dashboard.component.ts ***!
          \**************************************************************************/
        /*! exports provided: StudentDashboardComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentDashboardComponent", function () { return StudentDashboardComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var StudentDashboardComponent = /** @class */ (function () {
                function StudentDashboardComponent() {
                }
                StudentDashboardComponent.prototype.ngOnInit = function () {
                };
                return StudentDashboardComponent;
            }());
            StudentDashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-student-dashboard',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./student-dashboard.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/student/student-dashboard/student-dashboard.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./student-dashboard.component.css */ "./src/app/student/student-dashboard/student-dashboard.component.css")).default]
                })
            ], StudentDashboardComponent);
            /***/ 
        }),
        /***/ "./src/app/student/student-routing.module.ts": 
        /*!***************************************************!*\
          !*** ./src/app/student/student-routing.module.ts ***!
          \***************************************************/
        /*! exports provided: StudentRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentRoutingModule", function () { return StudentRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _student_can_activate__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./student-can-activate */ "./src/app/student/student-can-activate.ts");
            /* harmony import */ var _student_signup_student_signup_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./student-signup/student-signup.component */ "./src/app/student/student-signup/student-signup.component.ts");
            /* harmony import */ var _student_dashboard_student_dashboard_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./student-dashboard/student-dashboard.component */ "./src/app/student/student-dashboard/student-dashboard.component.ts");
            var routes = [
                { path: 'student/signup', component: _student_signup_student_signup_component__WEBPACK_IMPORTED_MODULE_4__["StudentSignupComponent"] },
                { path: 'student/dashboard', component: _student_dashboard_student_dashboard_component__WEBPACK_IMPORTED_MODULE_5__["StudentDashboardComponent"], canActivate: [_student_can_activate__WEBPACK_IMPORTED_MODULE_3__["StudentCanActivate"]] }
            ];
            var StudentRoutingModule = /** @class */ (function () {
                function StudentRoutingModule() {
                }
                return StudentRoutingModule;
            }());
            StudentRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
                    providers: [_student_can_activate__WEBPACK_IMPORTED_MODULE_3__["StudentCanActivate"]]
                })
            ], StudentRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/student/student-signup/student-signup.component.css": 
        /*!*********************************************************************!*\
          !*** ./src/app/student/student-signup/student-signup.component.css ***!
          \*********************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".full-width {\n  width: 300px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZGVudC9zdHVkZW50LXNpZ251cC9zdHVkZW50LXNpZ251cC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBWTtBQUNkIiwiZmlsZSI6InNyYy9hcHAvc3R1ZGVudC9zdHVkZW50LXNpZ251cC9zdHVkZW50LXNpZ251cC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZ1bGwtd2lkdGgge1xuICB3aWR0aDogMzAwcHg7XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/student/student-signup/student-signup.component.ts": 
        /*!********************************************************************!*\
          !*** ./src/app/student/student-signup/student-signup.component.ts ***!
          \********************************************************************/
        /*! exports provided: StudentSignupComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentSignupComponent", function () { return StudentSignupComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var src_app_service_student_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/student.service */ "./src/app/service/student.service.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var src_app_service_file_upload_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/service/file-upload.service */ "./src/app/service/file-upload.service.ts");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            var StudentSignupComponent = /** @class */ (function () {
                function StudentSignupComponent(formBuilder, studentService, router, fileUploadService) {
                    this.formBuilder = formBuilder;
                    this.studentService = studentService;
                    this.router = router;
                    this.fileUploadService = fileUploadService;
                    this.unknownError = false;
                    this.userAlreadyExistsError = false;
                    this.studentForm = this.formBuilder.group({
                        studentId: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[0-9]+')])],
                        name: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                        lastname: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                        email: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email])],
                        password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                        image: [''],
                        confirmPassword: ['']
                    }, { validator: this.checkPasswords });
                }
                StudentSignupComponent.prototype.checkPasswords = function (group) {
                    var pass = group.get('password');
                    var confirmPass = group.get('confirmPassword');
                    if (pass.value !== confirmPass.value) {
                        confirmPass.setErrors({ mustMatch: true });
                    }
                    else {
                        confirmPass.setErrors(null);
                    }
                };
                StudentSignupComponent.prototype.fieldHasError = function (fieldName, errorType) {
                    return this.studentForm.get(fieldName).hasError(errorType) &&
                        (this.studentForm.get(fieldName).dirty || this.studentForm.get(fieldName).touched);
                };
                StudentSignupComponent.prototype.submit = function (_a) {
                    var _this = this;
                    var value = _a.value, valid = _a.valid;
                    var password = value.password;
                    var student = {
                        email: value.email,
                        studentId: value.studentId,
                        name: value.name,
                        lastName: value.lastName,
                        image: value.image,
                    };
                    this.studentService.signup(student, password).then(function () {
                        _this.router.navigate(['/login'], { queryParams: { cmd: 'showSignupSuccess' } });
                    }, function (err) {
                        if (err.error != null && err.error.message == "User with this email already exists") {
                            _this.userAlreadyExistsError = true;
                        }
                        else {
                            _this.unknownError = true;
                        }
                    });
                };
                StudentSignupComponent.prototype.onUploadClicked = function (files) {
                    var _this = this;
                    console.log(typeof (files));
                    console.log(files.item(0));
                    var uploadedFile = files.item(0);
                    this.progress = 0;
                    this.fileUploadService.uploadFile(uploadedFile)
                        .subscribe(function (event) {
                        switch (event.type) {
                            case _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpEventType"].Sent:
                                console.log('Request has been made!');
                                break;
                            case _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpEventType"].ResponseHeader:
                                console.log('Response header has been received!');
                                break;
                            case _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpEventType"].UploadProgress:
                                _this.progress = Math.round(event.loaded / event.total * 100);
                                console.log("Uploaded! " + _this.progress + "%");
                                break;
                            case _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpEventType"].Response:
                                console.log('User successfully created!', event.body);
                                _this.uploadedUrl = event.body;
                                _this.studentForm.patchValue({
                                    image: _this.uploadedUrl
                                });
                                _this.studentForm.get('image').updateValueAndValidity();
                                setTimeout(function () {
                                    _this.progress = 0;
                                }, 1500);
                        }
                    });
                };
                StudentSignupComponent.prototype.ngOnInit = function () {
                };
                return StudentSignupComponent;
            }());
            StudentSignupComponent.ctorParameters = function () { return [
                { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] },
                { type: src_app_service_student_service__WEBPACK_IMPORTED_MODULE_3__["StudentService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
                { type: src_app_service_file_upload_service__WEBPACK_IMPORTED_MODULE_5__["FileUploadService"] }
            ]; };
            StudentSignupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
                    selector: 'app-student-signup',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./student-signup.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/student/student-signup/student-signup.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./student-signup.component.css */ "./src/app/student/student-signup/student-signup.component.css")).default]
                })
            ], StudentSignupComponent);
            /***/ 
        }),
        /***/ "./src/app/teacher/enrollment-approve/enrollment-approve.component.css": 
        /*!*****************************************************************************!*\
          !*** ./src/app/teacher/enrollment-approve/enrollment-approve.component.css ***!
          \*****************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlYWNoZXIvZW5yb2xsbWVudC1hcHByb3ZlL2Vucm9sbG1lbnQtYXBwcm92ZS5jb21wb25lbnQuY3NzIn0= */");
            /***/ 
        }),
        /***/ "./src/app/teacher/enrollment-approve/enrollment-approve.component.ts": 
        /*!****************************************************************************!*\
          !*** ./src/app/teacher/enrollment-approve/enrollment-approve.component.ts ***!
          \****************************************************************************/
        /*! exports provided: EnrollmentApproveComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnrollmentApproveComponent", function () { return EnrollmentApproveComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
            /* harmony import */ var src_app_service_enrollment_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/enrollment.service */ "./src/app/service/enrollment.service.ts");
            var EnrollmentApproveComponent = /** @class */ (function () {
                function EnrollmentApproveComponent(enrollmentService) {
                    this.enrollmentService = enrollmentService;
                    this.displayedColumns = ['studentName', 'approve', 'reject', 'undo'];
                    this.processingEnrollments = new Set();
                }
                Object.defineProperty(EnrollmentApproveComponent.prototype, "activityId", {
                    get: function () {
                        return this._activityId;
                    },
                    set: function (value) {
                        this._activityId = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                EnrollmentApproveComponent.prototype.isProcessing = function (enrollment) {
                    return this.processingEnrollments.has(enrollment);
                };
                EnrollmentApproveComponent.prototype.approve = function (enrollment) {
                    var _this = this;
                    this.processingEnrollments.add(enrollment);
                    this.enrollmentService.approveEnrollment(enrollment.enrollmentId).then(function () {
                        enrollment.isWaitingForApproval = false;
                        enrollment.isApproved = true;
                        _this.processingEnrollments.delete(enrollment);
                    }, function () {
                        _this.processingEnrollments.delete(enrollment);
                    });
                };
                EnrollmentApproveComponent.prototype.reject = function (enrollment) {
                    var _this = this;
                    this.processingEnrollments.add(enrollment);
                    this.enrollmentService.rejectEnrollment(enrollment.enrollmentId).then(function () {
                        enrollment.isWaitingForApproval = false;
                        enrollment.isApproved = false;
                        _this.processingEnrollments.delete(enrollment);
                    }, function () {
                        _this.processingEnrollments.delete(enrollment);
                    });
                };
                EnrollmentApproveComponent.prototype.undo = function (enrollment) {
                    enrollment.isWaitingForApproval = true;
                };
                EnrollmentApproveComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.enrollmentService.getActivityEnrollments(this.activityId).then(function (enrollment) {
                        _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](enrollment);
                        _this.dataSource.paginator = _this.paginator;
                        _this.dataSource.sort = _this.sort;
                    });
                };
                return EnrollmentApproveComponent;
            }());
            EnrollmentApproveComponent.ctorParameters = function () { return [
                { type: src_app_service_enrollment_service__WEBPACK_IMPORTED_MODULE_3__["EnrollmentService"] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"], null)
            ], EnrollmentApproveComponent.prototype, "paginator", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"], null)
            ], EnrollmentApproveComponent.prototype, "sort", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
            ], EnrollmentApproveComponent.prototype, "activityId", null);
            EnrollmentApproveComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-enrollment-approve',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./enrollment-approve.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/teacher/enrollment-approve/enrollment-approve.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./enrollment-approve.component.css */ "./src/app/teacher/enrollment-approve/enrollment-approve.component.css")).default]
                })
            ], EnrollmentApproveComponent);
            /***/ 
        }),
        /***/ "./src/app/teacher/teacher-can-activate.ts": 
        /*!*************************************************!*\
          !*** ./src/app/teacher/teacher-can-activate.ts ***!
          \*************************************************/
        /*! exports provided: TeacherCanActivate */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeacherCanActivate", function () { return TeacherCanActivate; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _service_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/authentication.service */ "./src/app/service/authentication.service.ts");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            var TeacherCanActivate = /** @class */ (function () {
                function TeacherCanActivate(authenticationService, router) {
                    this.authenticationService = authenticationService;
                    this.router = router;
                }
                TeacherCanActivate.prototype.canActivate = function (route, state) {
                    var _this = this;
                    var result = null;
                    if (this.authenticationService.isAuthorized) {
                        if (this.authenticationService.isTeacher) {
                            result = true;
                        }
                        else {
                            result = this.router.parseUrl('/authorize');
                        }
                    }
                    else {
                        result = Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["from"])(this.authenticationService.authorize().then(function () {
                            var innerResult = null;
                            if (_this.authenticationService.isTeacher) {
                                innerResult = true;
                            }
                            else {
                                innerResult = _this.router.parseUrl('/authorize');
                            }
                            return innerResult;
                        }));
                    }
                    return result;
                };
                return TeacherCanActivate;
            }());
            TeacherCanActivate.ctorParameters = function () { return [
                { type: _service_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
            ]; };
            TeacherCanActivate = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])()
            ], TeacherCanActivate);
            /***/ 
        }),
        /***/ "./src/app/teacher/teacher-dashboard/teacher-dashboard.component.css": 
        /*!***************************************************************************!*\
          !*** ./src/app/teacher/teacher-dashboard/teacher-dashboard.component.css ***!
          \***************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlYWNoZXIvdGVhY2hlci1kYXNoYm9hcmQvdGVhY2hlci1kYXNoYm9hcmQuY29tcG9uZW50LmNzcyJ9 */");
            /***/ 
        }),
        /***/ "./src/app/teacher/teacher-dashboard/teacher-dashboard.component.ts": 
        /*!**************************************************************************!*\
          !*** ./src/app/teacher/teacher-dashboard/teacher-dashboard.component.ts ***!
          \**************************************************************************/
        /*! exports provided: TeacherDashboardComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeacherDashboardComponent", function () { return TeacherDashboardComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var TeacherDashboardComponent = /** @class */ (function () {
                function TeacherDashboardComponent() {
                }
                TeacherDashboardComponent.prototype.ngOnInit = function () {
                };
                return TeacherDashboardComponent;
            }());
            TeacherDashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-teacher-dashboard',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./teacher-dashboard.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/teacher/teacher-dashboard/teacher-dashboard.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./teacher-dashboard.component.css */ "./src/app/teacher/teacher-dashboard/teacher-dashboard.component.css")).default]
                })
            ], TeacherDashboardComponent);
            /***/ 
        }),
        /***/ "./src/app/teacher/teacher-routing.module.ts": 
        /*!***************************************************!*\
          !*** ./src/app/teacher/teacher-routing.module.ts ***!
          \***************************************************/
        /*! exports provided: TeacherRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeacherRoutingModule", function () { return TeacherRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _teacher_can_activate__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./teacher-can-activate */ "./src/app/teacher/teacher-can-activate.ts");
            /* harmony import */ var _teacher_dashboard_teacher_dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./teacher-dashboard/teacher-dashboard.component */ "./src/app/teacher/teacher-dashboard/teacher-dashboard.component.ts");
            var routes = [
                { path: 'teacher/dashboard', component: _teacher_dashboard_teacher_dashboard_component__WEBPACK_IMPORTED_MODULE_4__["TeacherDashboardComponent"], canActivate: [_teacher_can_activate__WEBPACK_IMPORTED_MODULE_3__["TeacherCanActivate"]] }
            ];
            var TeacherRoutingModule = /** @class */ (function () {
                function TeacherRoutingModule() {
                }
                return TeacherRoutingModule;
            }());
            TeacherRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
                    providers: [_teacher_can_activate__WEBPACK_IMPORTED_MODULE_3__["TeacherCanActivate"]]
                })
            ], TeacherRoutingModule);
            /***/ 
        }),
        /***/ "./src/environments/environment.ts": 
        /*!*****************************************!*\
          !*** ./src/environments/environment.ts ***!
          \*****************************************/
        /*! exports provided: environment */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function () { return environment; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            // This file can be replaced during build by using the `fileReplacements` array.
            // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
            // The list of file replacements can be found in `angular.json`.
            var environment = {
                production: false,
            };
            /*
             * For easier debugging in development mode, you can import the following file
             * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
             *
             * This import should be commented out in production mode because it will have a negative impact
             * on performance if an error is thrown.
             */
            // import 'zone.js/dist/zone-error';  // Included with Angular CLI.
            /***/ 
        }),
        /***/ "./src/main.ts": 
        /*!*********************!*\
          !*** ./src/main.ts ***!
          \*********************/
        /*! no exports provided */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
            /* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/ __webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_1__);
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
            /* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
            /* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
            if (_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].production) {
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["enableProdMode"])();
            }
            Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_4__["AppModule"])
                .catch(function (err) { return console.error(err); });
            /***/ 
        }),
        /***/ 0: 
        /*!***************************!*\
          !*** multi ./src/main.ts ***!
          \***************************/
        /*! no static exports found */
        /***/ (function (module, exports, __webpack_require__) {
            module.exports = __webpack_require__(/*! /Users/ricofe25/Documents/Xingxing/ltas-frontend/src/main.ts */ "./src/main.ts");
            /***/ 
        })
    }, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es2015.js.map
//# sourceMappingURL=main-es5.js.map
//# sourceMappingURL=main-es5.js.map